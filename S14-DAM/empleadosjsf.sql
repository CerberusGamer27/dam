create database empresajsf;
use empresajsf;

CREATE TABLE departamento(
	codigoDepartamento INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    cantidadEmpleado INT(11) NOT NULL,
    PRIMARY KEY (codigoDepartamento)
);

CREATE TABLE empleado (
	codigoEmpleado INT(11) NOT NULL AUTO_INCREMENT,
    codigoDepartamento INT(11) NOT NULL,
    nombre VARCHAR(250) NOT NULL,
    genero VARCHAR(250) NOT NULL,
    intereses VARCHAR(250) NOT NULL,
    edad INT(11) NOT NULL,
    direccion VARCHAR(250) NOT NULL,
    cargo VARCHAR(250) NOT NULL,
    PRIMARY KEY (codigoEmpleado)
);

ALTER TABLE empleado ADD CONSTRAINT FK_EmpleadoDepartamento FOREIGN KEY (codigoDepartamento) REFERENCES departamento(codigoDepartamento);

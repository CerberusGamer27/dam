<%-- 
    Document   : index
    Created on : 9 sep. 2022, 19:15:28
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <jsp:include page="WEB-INF/recursoPrivado.jsp">
        <jsp:param name="colorFondo" value="blue"/>
    </jsp:include>
    <br/>
    <jsp:include page="paginas/recursoPublico.jsp" />
</html>

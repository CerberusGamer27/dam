/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package claseconexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class  Name: Conexion
 * Date: 26 sep. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Conexion {
    private Connection con;

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
    
     public String conectar() throws SQLException{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //con = DriverManager.getConnection("jdbc:mysql://141.148.161.172:3306/stocktaking?zeroDateTimeBehavior=CONVERT_TO_NULL");
            con = DriverManager.getConnection("jdbc:mysql://141.148.161.172:3306/stocktaking", "cerberus", "pass");
            return "Conexión exitosa";
            
        } catch (ClassNotFoundException | SQLException e) {
             e.getMessage();
            return "Error al conectar: "+e.getMessage();
        }
    }
    
      public String desconectar() throws SQLException {
        try {
            if (con != null) {
                if (con.isClosed() == false) {
                    con.close(); 
                }
            }
            return "Desconecto Exitosamente";
        } catch (SQLException e) {
            e.getMessage();
            return "Error al desconectar"+e.getMessage();
        }
    }
}

<%-- 
    Document   : getJsp
    Created on : 9 sep. 2022, 18:45:18
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Obtener Valores</h1>
        <jsp:useBean id="numeros" class="com.beans.Numeros" scope="session" />
        Numero 1: <jsp:getProperty name="numeros" property="n1" /><br>
        Numero 2: <jsp:getProperty name="numeros" property="n2" /><br>
        Resultado: <jsp:getProperty name="numeros" property="producto" /><br>
        <a href="index.jsp">Regresar al index</a>
    </body>
</html>

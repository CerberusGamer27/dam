<%-- 
    Document   : setJsp
    Created on : 9 sep. 2022, 18:44:55
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Modificando Valores JB</h1>
        <jsp:useBean id="numeros" class="com.beans.Numeros" scope="session" />
        <%
            double a=10;
            double b=10;
        %>
        <jsp:setProperty name="numeros" property="n1" value="<%= a %>" />
        <jsp:setProperty name="numeros" property="n2" value="<%= b %>" />
        Numero 1: <%= a%><br>
        Numero 2: <%= b%><br>
        <a href="index.jsp">Regresar al index</a>
    </body>
</html>

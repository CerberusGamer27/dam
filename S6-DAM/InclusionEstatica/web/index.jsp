<%-- 
    Document   : index
    Created on : 9 sep. 2022, 19:06:24
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ejemplo de Inclusion Estatica</h1>
        <br>
        
        <li><%@include file="/paginas/noticias1.html" %></li>
        <li><%@include file="/paginas/noticias2.jsp" %></li>
    </body>
</html>

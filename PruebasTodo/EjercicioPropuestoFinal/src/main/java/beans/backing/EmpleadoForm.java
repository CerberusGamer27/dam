package beans.backing;


import beans.model.Empleado;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Named
@RequestScoped
public class EmpleadoForm extends Object {

    @Inject
    private Empleado empleado;

    private boolean comentarioEnviado;

    Logger log = LogManager.getRootLogger();

    public EmpleadoForm() {
        log.info("Creando el objeto VacanteForm");
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    public void ocultarComentario(ActionEvent actionEvent){
        this.comentarioEnviado = !this.comentarioEnviado;
    }

    public boolean isComentarioEnviado() {
        return comentarioEnviado;
    }

    public void setComentarioEnviado(boolean comentarioEnviado) {
        this.comentarioEnviado = comentarioEnviado;
    }
}

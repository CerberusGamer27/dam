/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package beans.backing;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import beans.model.Empleado;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author rober
 */
@Named
@RequestScoped
public class EmpleadoForm {

    @Inject
    private Empleado empleado;
    private boolean seleccionarHabilidades=true;

    public EmpleadoForm() {
    }

    public String enviar() {
        return "datos";
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public boolean isSeleccionarHabilidades() {
        return seleccionarHabilidades;
    }

    public void setSeleccionarHabilidades(boolean seleccionarHabilidades) {
        this.seleccionarHabilidades = seleccionarHabilidades;
    }

    public void ocultarHabilidades(ActionEvent actionEvent) {
        this.seleccionarHabilidades = false;
    }
    
    public void mostrarHabilidades(ActionEvent actionEvent){
        this.seleccionarHabilidades=true;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package com.beans;

import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author C3rberus
 */
@Named(value = "newJSFManagedBean")
@Dependent
public class NewJSFManagedBean {

    /**
     * Creates a new instance of NewJSFManagedBean
     */
    public NewJSFManagedBean() {
    }
    
}

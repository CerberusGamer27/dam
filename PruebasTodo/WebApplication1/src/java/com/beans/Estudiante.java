/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Class  Name: Estudiante
 * Date: 3 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
@Named
@RequestScoped
public class Estudiante {
    private String nombre = "Hola";

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}

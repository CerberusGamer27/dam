/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.beans;

import com.dao.UsuarioDAO;
import com.modelo.Usuario;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * Class Name: IndexBean Date: 5 nov. 2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
@Named
@ViewScoped
public class IndexBean implements Serializable {
    private Usuario us;
    
    public IndexBean () {
        us = new Usuario();
    }
    
    public Usuario getUs() {
        return us;
    }
    
    public void setUs(Usuario usuario) {
        this.us = usuario;
    }
    
    public String iniciarSesion() {
        Usuario us;
        String redireccion = null;
        UsuarioDAO daoUsuario;
        daoUsuario = new UsuarioDAO();
        try {
            us = daoUsuario.iniciarSesion(this.us);
            if (us != null) {     
                //Almacenar en la sesión de JSF
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
                redireccion = "mainTemplate?faces-redirect=true";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error!"));
        }
        return redireccion;
    }
    
    
}

package com.modelo;

/**
 * Class  Name: Usuario
 * Date: 5 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Usuario {
    private String username;
    private String contra;
    private String rol;

    public Usuario() {
    }

    public Usuario(String username, String contra, String rol) {
        this.username = username;
        this.contra = contra;
        this.rol = rol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContra() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
    
}

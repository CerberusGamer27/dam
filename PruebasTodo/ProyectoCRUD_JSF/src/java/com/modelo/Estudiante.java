/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.modelo;

/**
 * Class  Name: Estudiante
 * Date: 4 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Estudiante {
    private int codigoEstudiante;
    private String nombre;
    private int edad;
    private double cum;
    private Carrera carrera = new Carrera();

    public Estudiante() {
        carrera = new Carrera();
        carrera.setCodigoCarrera(0);
    }

    public Estudiante(int codigoEstudiante, String nombre, int edad, double cum) {
        this.codigoEstudiante = codigoEstudiante;
        this.nombre = nombre;
        this.edad = edad;
        this.cum = cum;
    }

    public int getCodigoEstudiante() {
        return codigoEstudiante;
    }

    public void setCodigoEstudiante(int codigoEstudiante) {
        this.codigoEstudiante = codigoEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getCum() {
        return cum;
    }

    public void setCum(double cum) {
        this.cum = cum;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }
}

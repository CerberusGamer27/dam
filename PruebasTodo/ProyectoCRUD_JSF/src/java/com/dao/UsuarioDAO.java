/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.dao;

import com.conexion.Conexion;
import com.modelo.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * Class  Name: UsuarioDAO
 * Date: 5 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class UsuarioDAO extends Conexion {
    
    public Usuario iniciarSesion(Usuario us) {
        Usuario usuario = null;
        String sql;
        ResultSet rs;
        PreparedStatement ps;
        try {
            sql = "select * from usuario where username=? and contra=?";
            ps = this.getConn().prepareStatement(sql);
            ps.setString(1, us.getUsername());
            ps.setString(2, us.getContra());
            rs = ps.executeQuery();
            
            rs.next();
            us.setUsername(rs.getString("username"));
            us.setContra(rs.getString("contra"));
            us.setRol(rs.getString("rol"));
            ps.close();
            rs.close();
        } catch (Exception e) {
        }       
        return usuario;
    }
}

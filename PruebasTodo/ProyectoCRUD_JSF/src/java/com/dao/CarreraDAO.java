package com.dao;

import com.conexion.Conexion;
import com.modelo.Carrera;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Name: CarreraDAO Date: 4 nov. 2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
public class CarreraDAO extends Conexion {

    public void insertar(Carrera carrera) {
        String sql;
        PreparedStatement ps;
        try {
            if (this.conectar()) {
                sql = "INSERT INTO carrera(nombre,cantidadMaterias) VALUES(?,?) ";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, carrera.getNombre());
                ps.setInt(2, carrera.getCantidadMaterias());
                ps.executeUpdate();
                System.out.println("Exito al insertar");
            } else {
                System.out.println("Error al insertar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
    }

    public void eliminar(Carrera carrera) {
        String sql;
        PreparedStatement ps;
        try {
            if (this.conectar()) {
                sql = "DELETE FROM carrera WHERE codigoCarrera = ?";
                ps = this.getConn().prepareStatement(sql);
                ps.setInt(1, carrera.getCodigoCarrera());
                ps.executeUpdate();
                System.out.println("Exito al eliminar");
            } else {
                System.out.println("Error al Eliminar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
    }

    public void modificar(Carrera carrera) {
        String sql;
        PreparedStatement ps;
        try {
            if (this.conectar()) {
                sql = "UPDATE carrera SET nombre = ?, cantidadMaterias = ? WHERE codigoCarrera = ?";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, carrera.getNombre());
                ps.setInt(2, carrera.getCantidadMaterias());
                ps.setInt(3, carrera.getCodigoCarrera());
                ps.executeUpdate();
                System.out.println("Exito al actualizar");
            } else {
                System.out.println("Error al actualizar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
    }

    public List<Carrera> mostrar() {
        List<Carrera> carreras = new ArrayList<>();
        try {
            String sql;
            PreparedStatement ps;
            ResultSet rs;
            Carrera car;
            if (this.conectar()) {
                sql = "SELECT * FROM carrera";
                ps = this.getConn().prepareCall(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    car = new Carrera();
                    car.setCodigoCarrera(rs.getInt("codigoCarrera"));
                    car.setNombre(rs.getString("nombre"));
                    car.setCantidadMaterias(rs.getInt("cantidadMaterias"));
                    carreras.add(car);
                }
            } else {
                System.out.println("Error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return carreras;
    }

    public Carrera leerId(Carrera carrera) {
        String sql;
        PreparedStatement ps;
        Carrera car = null;
        ResultSet rs;
        try {
            if (this.conectar()) {
                sql = "SELECT * FROM carrera WHERE codigoCarrera = ? ";
                ps = this.getConn().prepareCall(sql);
                ps.setInt(1, carrera.getCodigoCarrera());
                rs = ps.executeQuery();

                while (rs.next()) {
                    car = new Carrera();
                    car.setCodigoCarrera(rs.getInt("codigoCarrera"));
                    car.setNombre(rs.getString("nombre"));
                    car.setCantidadMaterias(rs.getInt("cantidadMaterias"));
                }
            } else {
                System.out.println("Error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return car;
    }
}

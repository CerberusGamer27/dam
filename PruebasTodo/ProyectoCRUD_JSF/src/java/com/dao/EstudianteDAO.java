/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dao;

import com.conexion.Conexion;
import com.modelo.Carrera;
import com.modelo.Estudiante;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Name: EstudianteDAO Date: 4 nov. 2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
public class EstudianteDAO extends Conexion {

    public int insertarDb(Estudiante est) {
        String sql;
        PreparedStatement ps;
        int executeUpdate = 0;
        try {
            if (this.conectar()) {
                sql = "INSERT INTO estudiante(nombre,edad,cum,codigoCarrera) VALUES(?,?,?,?) ";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, est.getNombre());
                ps.setInt(2, est.getEdad());
                ps.setDouble(3, est.getCum());
                ps.setInt(4, est.getCarrera().getCodigoCarrera());

                executeUpdate = ps.executeUpdate();
                ps.close();
                System.out.println("Exito al insertar");
            } else {
                System.out.println("Error al insertar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
        return executeUpdate;
    }

    public int eliminarDb(Estudiante estudiante) {
        String sql;
        PreparedStatement ps;
        int executeUpdate = 0;
        try {
            if (this.conectar()) {
                sql = "DELETE FROM estudiante WHERE codigoEstudiante = ?";
                ps = this.getConn().prepareStatement(sql);

                ps.setInt(1, estudiante.getCodigoEstudiante());
                executeUpdate = ps.executeUpdate();
                System.out.println("Exito al eliminar");
            } else {
                System.out.println("Error al Eliminar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
        return executeUpdate;
    }

    public int modificarDB(Estudiante es) {
        String sql;
        PreparedStatement ps;
        int executeUpdate = 0;
        try {
            if (this.conectar()) {
                sql = "UPDATE estudiante SET nombre = ?, edad = ?, cum = ?, codigoCarrera = ? WHERE codigoEstudiante = ?";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, es.getNombre());
                ps.setInt(2, es.getEdad());
                ps.setDouble(3, es.getCum());
                ps.setInt(4, es.getCarrera().getCodigoCarrera());
                ps.setInt(5, es.getCodigoEstudiante());
                executeUpdate = executeUpdate = ps.executeUpdate();
                System.out.println("Exito al actualizar");
            } else {
                System.out.println("Error al actualizar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
        return executeUpdate;
    }

    public ArrayList<Estudiante> listarDb() {
        String sql;
        PreparedStatement ps;
        ArrayList<Estudiante> estudiantes = new ArrayList<>();
        Carrera car;
        try {
            ResultSet rs;
            Estudiante es;
            if (this.conectar()) {
                sql = "SELECT es.codigoEstudiante, es.nombre, es.nombre, es.edad, es.cum, ca.codigoCarrera, ca.nombre AS nombreCarrera "
                        + "FROM estudiante es INNER JOIN carrera ca ON es.codigoCarrera = ca.codigoCarrera;";
                ps = this.getConn().prepareCall(sql);

                rs = ps.executeQuery();
                while (rs.next()) {
                    car = new Carrera();
                    car.setNombre(rs.getString("nombreCarrera"));
                    car.setCodigoCarrera(rs.getInt("codigoCarrera"));

                    es = new Estudiante();
                    es.setCodigoEstudiante(rs.getInt("codigoEstudiante"));
                    es.setNombre(rs.getString("nombre"));
                    es.setEdad(rs.getInt("edad"));
                    es.setCum(rs.getDouble("cum"));
                    es.setCarrera(car);
                    estudiantes.add(es);
                }
                rs.close();
                ps.close();
            } else {

                System.out.println("Error");
            }
        } catch (SQLException e) {
            estudiantes = null;
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return estudiantes;
    }

    public Estudiante buscarOjetoDb(Estudiante estudiante) {
        String sql;
        PreparedStatement ps;
        Estudiante es = null;
        ResultSet rs;
        Carrera car;
        try {
            if (this.conectar()) {
                sql = "SELECT es.codigoEstudiante, es.nombre, es.nombre, es.edad, es.cum, ca.codigoCarrera, ca.nombre AS nombreCarrera "
                        + "FROM estudiante es INNER JOIN carrera ca ON es.codigoCarrera = ca.codigoCarrera where es.codigoEstudiante = ?;";
                ps = this.getConn().prepareCall(sql);
                ps.setInt(1, estudiante.getCodigoEstudiante());
                rs = ps.executeQuery();

                rs.next();
                car = new Carrera();
                car.setNombre(rs.getString("nombreCarrera"));
                car.setCodigoCarrera(rs.getInt("codigoCarrera"));

                es = new Estudiante();
                es.setCodigoEstudiante(rs.getInt("codigoEstudiante"));
                es.setNombre(rs.getString("nombre"));
                es.setEdad(rs.getInt("edad"));
                es.setCum(rs.getDouble("cum"));
                es.setCarrera(car);

                rs.close();
                ps.close();
            } else {
                System.out.println("Error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return es;
    }
}

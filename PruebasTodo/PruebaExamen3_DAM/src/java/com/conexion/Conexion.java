package com.conexion;

//import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class Name: Conexion Date: 4 nov. 2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
public class Conexion {

    private Connection conn;

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public boolean conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/examen3", "root", "");
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean desconectar(){
        try{
            if(this.conn != null){
                if(!this.conn.isClosed()){
                    this.conn.close();
                }
            }
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;     
        }
    }
}

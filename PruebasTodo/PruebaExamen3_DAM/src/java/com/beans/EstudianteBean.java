package com.beans;

import com.dao.EstudianteDAO;
import com.modelo.Estudiante;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * Class  Name: EstudianteBean
 * Date: 5 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
@Named(value = "empleadoBean")
@SessionScoped
public class EstudianteBean implements Serializable {
    private Estudiante es;
    private ArrayList<Estudiante> listaEstudiante;
    
    public EstudianteBean() {
        es = new Estudiante();
    }
    
    public Estudiante getEs(){
        return es;
    }

    public void setEs(Estudiante estudiante){
        this.es = estudiante;
    }
    
    public ArrayList<Estudiante> getListaEmpleados() {
        return listaEstudiante;
    }

    public void setListaEmpleados(ArrayList<Estudiante> listEstudiante) {
        this.listaEstudiante = listEstudiante;
    }
    
    public void insertarEmpleado(){
        EstudianteDAO daoEs;
        
        daoEs = new EstudianteDAO();
        daoEs.insertarDb(this.es);
        
        listarEmpleados();
        this.es = new Estudiante();
        
        //mostrando menaje
        FacesContext context = FacesContext.getCurrentInstance();
        
        context.addMessage(null, new FacesMessage("Exito","Estudiante insertado correctamente"));
    }
    
    public void modificarEmpleado(){
        
        EstudianteDAO daoEs;
        
        daoEs = new EstudianteDAO();
        daoEs.modificarDb(this.es);
        
        listarEmpleados();
        this.es = new Estudiante();
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Exito","Estudiante modificado correctamente"));
    }

    public void listarEmpleados(){
        EstudianteDAO daoEs;
        
        daoEs = new EstudianteDAO();
        this.listaEstudiante = daoEs.listarDb();
        this.es = new Estudiante();
    }
    
    public void delete(Estudiante base){
        EstudianteDAO daoEs;
        Estudiante prueba;
        int id;

        daoEs = new EstudianteDAO();
        id = base.getCodigoEstudiante();
        
        prueba = new Estudiante();
        prueba.setCodigoEstudiante(id);
        
        daoEs.eliminarDb(base);
        
        this.listarEmpleados();
        this.es = new Estudiante();
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Exito", "Empleado eliminado correctamente"));
    }
    
    public void buscarEmpleado(Estudiante base){
        EstudianteDAO daoEs;
        Estudiante es;

        daoEs = new EstudianteDAO();
        es = new Estudiante();
        es.setCodigoEstudiante(base.getCodigoEstudiante());
        
        this.es = daoEs.buscarOjetoDb(es);
    }
    
}

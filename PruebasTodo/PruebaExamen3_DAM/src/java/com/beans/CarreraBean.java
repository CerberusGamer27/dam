/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.beans;

import com.dao.CarreraDAO;
import com.modelo.Carrera;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Class  Name: CarreraBean
 * Date: 4 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
@ManagedBean
@ViewScoped
public class CarreraBean {
    
    public CarreraBean() {
        
    }
    
    private Carrera car = new Carrera();
    private List<Carrera> listarCarreras;
    CarreraDAO carreraDao = new CarreraDAO();
    
    public Carrera getCarrera() {
        return car;
    }
    
    public void setCarrera(Carrera car){
        this.car = car;
    }
    
    public List<Carrera> getListarCarreras(){
        return listarCarreras;
    }
    
    public void setListarCarreras(List<Carrera> listarCarreras){
        this.listarCarreras = listarCarreras;
    }
    
    //Metodos
    public void registar(){
        try {
            carreraDao.insert(car);
            car = new Carrera();
            
            addMessage("Exito", "Carrera Insertada Correctamente");
        } catch (Exception e) {
        }
    }
    
    public void listarC(){
        try {
            listarCarreras = carreraDao.getAllCarreras();
        } catch (Exception e) {
        }
    }
    
    public void seleccionar (Carrera carre){
        Carrera carr;
        try {
            carr = carreraDao.getById(carre);
            if(carr != null){
                this.car = carr;
            }
        } catch (Exception e){
        }
    }
    
    public void modificar(){
        try {
            carreraDao.edit(car);
            this.listarC();
            addMessage("Exito", "Carrera modificado correctamente");
        } catch (Exception e) {
        }
    }
    
    public void eliminar(Carrera carr){
        try {
            carreraDao.delete(carr);
            addMessage("Exito", "Carrera eliminado correctamente.");
            this.listarC();
        } catch (Exception e) {
        }
    }
    
    
    
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}

package com.modelo;

/**
 * Class  Name: Carrera
 * Date: 4 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Carrera {
    private int codigoCarrera;
    private String nombre;
    private int cantidadMaterias;

    public Carrera() {
    }

    public Carrera(int codigoCarrera, String nombre, int canttidadMaterias) {
        this.codigoCarrera = codigoCarrera;
        this.nombre = nombre;
        this.cantidadMaterias = canttidadMaterias;
    }

    public int getCodigoCarrera() {
        return codigoCarrera;
    }

    public void setCodigoCarrera(int codigoCarrera) {
        this.codigoCarrera = codigoCarrera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCanttidadMaterias() {
        return cantidadMaterias;
    }

    public void setCanttidadMaterias(int canttidadMaterias) {
        this.cantidadMaterias = canttidadMaterias;
    }
    
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package beans.model;

import java.util.Arrays;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author rober
 */
@Named
@RequestScoped
public class Estudiante {

    private String carnet;
    private String nombre;
    private String apellidos;
    private int anioNacimiento;
    private String carrera;
    private double nota1;
    private double nota2;
    private String[] materiasInscritas;
    private double promedio;
    private String estado;

    public Estudiante() {
    }

    public Estudiante(String carnet, String nombre, String apellidos, int anioNacimiento, String carrera, double nota1, double nota2, String[] materiasInscritas, double promedio, String estado) {
        this.carnet = carnet;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.anioNacimiento = anioNacimiento;
        this.carrera = carrera;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.materiasInscritas = materiasInscritas;
        this.promedio = promedio;
        this.estado = estado;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getAnioNacimiento() {
        return anioNacimiento;
    }

    public void setAnioNacimiento(int anioNacimiento) {
        this.anioNacimiento = anioNacimiento;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public String[] getMateriasInscritas() {
        return materiasInscritas;
    }

    public void setMateriasInscritas(String[] materiasInscritas) {
        this.materiasInscritas = materiasInscritas;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMateriasString() {
        return "Materias: " + Arrays.toString(materiasInscritas);
    }
}

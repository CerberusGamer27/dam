/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package beans.backing;

import javax.inject.Inject;
import beans.model.Estudiante;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author rober
 */
@Named
@RequestScoped
public class EstudianteForm extends Object {

    @Inject
    private Estudiante estudiante;

    public String enviar() {
        generarCarnet();
        calcularPromedioEstado();
        return "datos";
    }

    public EstudianteForm() {
    }

    private void generarCarnet() {
        int valorDado = (int) Math.floor(Math.random()*10+1);
        String carnet = String.format("%s%s%s",estudiante.getApellidos().charAt(0), estudiante.getNombre().charAt(0),valorDado );
        estudiante.setCarnet(carnet);
    }
    
    private void calcularPromedioEstado(){
        double promedio = (estudiante.getNota1()+estudiante.getNota2())/2;
        estudiante.setPromedio(promedio);
        if(promedio>=0 && promedio<2){
            estudiante.setEstado("Necesita mejorar");
        }else if(promedio>=2 && promedio<4){
            estudiante.setEstado("Malo");
        }else if(promedio>=4 && promedio<6){
            estudiante.setEstado("Regular");
        }else if(promedio>=6 && promedio<8){
            estudiante.setEstado("Muy Bueno");
        }else if(promedio>=8 && promedio<=10){
            estudiante.setEstado("Excelente");
        }
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

}

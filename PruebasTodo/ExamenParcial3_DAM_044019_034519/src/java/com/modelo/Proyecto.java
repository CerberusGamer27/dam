/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.modelo;

/**
 * Class  Name: Proyecto
 * Date: 5 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Roberto Marquez (044019)
 */
public class Proyecto {
    private int codigoProyecto;
    private String nombre;
    private String ubicacion;
    private String direccion;
    private String telefono;

    public Proyecto() {
    }

    public Proyecto(int codigoProyecto, String nombre, String ubicacion, String direccion, String telefono) {
        this.codigoProyecto = codigoProyecto;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public int getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(int codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}

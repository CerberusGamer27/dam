/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.modelo;

import java.util.List;

/**
 *
 * @author rober
 */
public class Coordinador {
    
    private int codigoCoordinador;
    private String nombre;
    private int edad;
    private double sueldoBase;
    private List<String> intereses;
    private String sexo;
    private String area;
    private Proyecto proyecto = new Proyecto();

    public Coordinador() {
        proyecto = new Proyecto();
        proyecto.setCodigoProyecto(0);
    }

    public Coordinador(int codigoCoordinador, String nombre, int edad, double sueldoBase, List<String> intereses, String sexo, String area) {
        this.codigoCoordinador = codigoCoordinador;
        this.nombre = nombre;
        this.edad = edad;
        this.sueldoBase = sueldoBase;
        this.intereses = intereses;
        this.sexo = sexo;
        this.area = area;
    }


    public int getCodigoCoordinador() {
        return codigoCoordinador;
    }

    public void setCodigoCoordinador(int codigoCoordinador) {
        this.codigoCoordinador = codigoCoordinador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getSueldoBase() {
        return sueldoBase;
    }

    public void setSueldoBase(double sueldoBase) {
        this.sueldoBase = sueldoBase;
    }

    public List<String> getIntereses() {
        return intereses;
    }

    public void setIntereses(List<String> intereses) {
        this.intereses = intereses;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }
    
    
    
    
}

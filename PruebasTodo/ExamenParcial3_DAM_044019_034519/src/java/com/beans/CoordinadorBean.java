/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.beans;

import com.dao.CoordinadorDAO;
import com.modelo.Coordinador;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author rober
 */
@Named(value = "coordinadorBean")
@SessionScoped
public class CoordinadorBean implements Serializable {

    private Coordinador coor;
    private List<Coordinador> listaCoordinador;

    public CoordinadorBean() {
        coor = new Coordinador();
    }

    public Coordinador getCoor() {
        return coor;
    }

    public void setCoor(Coordinador coordinador) {
        this.coor = coordinador;
    }

    public List<Coordinador> getListaCoordinador() {
        return listaCoordinador;
    }

    public void setListaCoordinador(List<Coordinador> listaCoordinador) {
        this.listaCoordinador = listaCoordinador;
    }

    public void insertarCoordinador() {
        CoordinadorDAO daoCoor;

        daoCoor = new CoordinadorDAO();
        daoCoor.insertarDb(this.coor);

        listarCoordinadores();
        this.coor = new Coordinador();

        //mostrando menaje
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Exito", "Coordinador insertado correctamente"));
    }

    public void modificarCoordinador() {

        CoordinadorDAO daoCoor;

        daoCoor = new CoordinadorDAO();
        daoCoor.modificarDB(this.coor);

        listarCoordinadores();
        this.coor = new Coordinador();

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Exito", "Coordinador modificado correctamente"));
    }

    public void listarCoordinadores() {
        CoordinadorDAO daoCoor;

        daoCoor = new CoordinadorDAO();
        this.listaCoordinador = daoCoor.listarDb();
        this.coor = new Coordinador();
    }

    public void delete(Coordinador base) {
        CoordinadorDAO daoCoor;
        Coordinador prueba;
        int id;

        daoCoor = new CoordinadorDAO();
        id = base.getCodigoCoordinador();

        prueba = new Coordinador();
        prueba.setCodigoCoordinador(id);

        daoCoor.eliminarDb(base);

        this.listarCoordinadores();
        this.coor = new Coordinador();

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Exito", "Coordinador eliminado correctamente"));
    }

    public void buscarCoordinador(Coordinador base) {
        CoordinadorDAO daoCoor;
        Coordinador coor;

        daoCoor = new CoordinadorDAO();
        coor = new Coordinador();
        coor.setCodigoCoordinador(base.getCodigoCoordinador());

        this.coor = daoCoor.buscarOjetoDb(coor);
    }
}

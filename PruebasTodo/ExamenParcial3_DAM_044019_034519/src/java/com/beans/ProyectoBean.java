/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.beans;

import com.dao.ProyectoDAO;
import com.modelo.Proyecto;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Class  Name: ProyectoBean
 * Date: 5 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
@ManagedBean
@ViewScoped
public class ProyectoBean {
    public ProyectoBean() {
    }

    private Proyecto pro = new Proyecto();
    private List<Proyecto> listarProyectos = new ArrayList<>();
    private ProyectoDAO proyectoDao = new ProyectoDAO();

    public Proyecto getPro(){
        return pro;
    }
    
    public void setPro(Proyecto pro) {
        this.pro = pro;
    }

    public List<Proyecto> getListarProyectos() {
        return listarProyectos;
    }

    public void setListarProyectos(List<Proyecto> listarProyectos) {
        this.listarProyectos = listarProyectos;
    }

    public void registrar() {
        try {
            proyectoDao.insertar(pro);

            pro = new Proyecto();

            addMessage("Exito", "Proyecto insertado Correctamente");
        } catch (Exception e) {
        }
    }

    public void listarP() {
        try {
            listarProyectos = proyectoDao.mostrar();
        } catch (Exception e) {
        }
    }

    public void seleccionar(Proyecto proyecto) {
        Proyecto proy;
        try {
            proy = proyectoDao.leerId(proyecto);
            if (proy != null) {
                this.pro = proy;
            }
        } catch (Exception e) {
        }
    }

    public void modificar() {
        try {
            proyectoDao.modificar(pro);
            this.listarP();
            addMessage("Exito", "Proyecto modificado correctamente");
        } catch (Exception e) {
        }
    }

    public void eliminar(Proyecto proyecto) {
        try {
            proyectoDao.eliminar(proyecto);
            addMessage("Exito", "Proyecto eliminado correctamente.");
            this.listarP();
        } catch (Exception e) {
        }
    }

    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}

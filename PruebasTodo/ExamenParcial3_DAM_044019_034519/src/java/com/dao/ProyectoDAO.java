/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.dao;

import com.conexion.Conexion;
import com.modelo.Proyecto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class  Name: ProyectoDAO
 * Date: 5 nov. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class ProyectoDAO extends Conexion {

    public void insertar(Proyecto proyecto) {
        String sql;
        PreparedStatement ps;
        try {
            if (this.conectar()) {
                sql = "INSERT INTO proyecto(nombre,ubicacion,direccion,telefono) VALUES (?,?,?,?)";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, proyecto.getNombre());
                ps.setString(2, proyecto.getUbicacion());
                ps.setString(3, proyecto.getDireccion());
                ps.setString(4, proyecto.getTelefono());
                ps.executeUpdate();
                System.out.println("Exito al insertar");
            } else {
                System.out.println("Error al insertar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
    }
    
    public void eliminar(Proyecto proyecto) {
        String sql;
        PreparedStatement ps;
        try {
            if (this.conectar()) {
                sql = "DELETE FROM proyecto WHERE codigoProyecto=?";
                ps = this.getConn().prepareStatement(sql);
                ps.setInt(1, proyecto.getCodigoProyecto());
                ps.executeUpdate();
                System.out.println("Exito al eliminar");
            } else {
                System.out.println("Error al Eliminar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
    }    
    
    
    public void modificar(Proyecto proyecto) {
        String sql;
        PreparedStatement ps;
        try {
            if (this.conectar()) {
                sql = "UPDATE proyecto SET nombre=?,ubicacion=?,direccion=?,telefono=? WHERE codigoProyecto=?";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, proyecto.getNombre());
                ps.setString(2, proyecto.getUbicacion());
                ps.setString(3, proyecto.getDireccion());
                ps.setString(4, proyecto.getTelefono());
                ps.setInt(5, proyecto.getCodigoProyecto());
                ps.executeUpdate();
                System.out.println("Exito al actualizar");
            } else {
                System.out.println("Error al actualizar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
    }    
    
    public List<Proyecto> mostrar() {
        List<Proyecto> proyectos = new ArrayList<>();
        try {
            String sql;
            PreparedStatement ps;
            ResultSet rs;
            Proyecto pro;
            if (this.conectar()) {
                sql = "SELECT * FROM proyecto";
                ps = this.getConn().prepareCall(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pro = new Proyecto();
                    System.out.println(rs.getInt(1));
                    pro.setCodigoProyecto(rs.getInt(1));
                    System.out.println(rs.getString(2));
                    pro.setNombre(rs.getString(2));
                    pro.setUbicacion(rs.getString(3));
                    pro.setDireccion(rs.getString(4));
                    pro.setTelefono(rs.getString(5));
                    proyectos.add(pro);
                }
            } else {
                System.out.println("Error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return proyectos;
    } 
    
    public Proyecto leerId(Proyecto proyecto) {
        String sql;
        PreparedStatement ps;
        Proyecto pro=null;
        ResultSet rs;
        try {
            if (this.conectar()) {
                sql = "SELECT * FROM proyecto WHERE codigoProyecto=? ";
                ps = this.getConn().prepareCall(sql);
                ps.setInt(1, proyecto.getCodigoProyecto());
                rs = ps.executeQuery();

                while (rs.next()) {
                    pro = new Proyecto();
                    pro.setCodigoProyecto(rs.getInt(1));
                    pro.setNombre(rs.getString(2));
                    pro.setUbicacion(rs.getString(3));
                    pro.setDireccion(rs.getString(4));
                    pro.setTelefono(rs.getString(5));
                }
            } else {
                System.out.println("Error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return pro;
    }    

}
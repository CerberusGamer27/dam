/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dao;

import com.conexion.Conexion;
import com.modelo.Coordinador;
import com.modelo.Proyecto;
import com.utilidades.UtilidadesVarias;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rober
 */
public class CoordinadorDAO extends Conexion {

    public int insertarDb(Coordinador coor) {
        String sql;
        PreparedStatement ps;
        int executeUpdate = 0;
        try {
            if (this.conectar()) {
                sql = "INSERT INTO coordinador(nombre,edad,sueldoBase,codigoProyecto,intereses,sexo,area) VALUES(?,?,?,?,?,?,?)";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, coor.getNombre());
                ps.setInt(2, coor.getEdad());
                ps.setDouble(3, coor.getSueldoBase());
                ps.setInt(4, coor.getProyecto().getCodigoProyecto());
                ps.setString(5, UtilidadesVarias.unirString((ArrayList<String>) coor.getIntereses()));
                ps.setString(6, coor.getSexo());
                ps.setString(7, coor.getArea());

                executeUpdate = ps.executeUpdate();
                ps.close();
                System.out.println("Exito al insertar");
            } else {
                System.out.println("Error al insertar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
        return executeUpdate;
    }

    public int eliminarDb(Coordinador coor) {
        String sql;
        PreparedStatement ps;
        int executeUpdate = 0;
        try {
            if (this.conectar()) {
                sql = "DELETE FROM coordinador WHERE codigoCoordinador = ?";
                ps = this.getConn().prepareStatement(sql);
                ps.setInt(1, coor.getCodigoCoordinador());
                executeUpdate = ps.executeUpdate();
                System.out.println("Exito al eliminar");
            } else {
                System.out.println("Error al Eliminar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
        return executeUpdate;
    }

    public int modificarDB(Coordinador coor) {
        String sql;
        PreparedStatement ps;
        int executeUpdate = 0;
        try {
            if (this.conectar()) {
                sql = "UPDATE coordinador SET nombre=?,edad=?,sueldoBase=?,codigoProyecto=?,intereses=?, sexo=?,area=? WHERE codigoCoordinador=?";
                ps = this.getConn().prepareStatement(sql);
                ps.setString(1, coor.getNombre());
                ps.setInt(2, coor.getEdad());
                ps.setDouble(3, coor.getSueldoBase());
                ps.setInt(4, coor.getProyecto().getCodigoProyecto());
                ps.setString(5, UtilidadesVarias.unirString((ArrayList<String>) coor.getIntereses()));
                ps.setString(6, coor.getSexo());
                ps.setString(7, coor.getArea());
                ps.setInt(8, coor.getCodigoCoordinador());
                executeUpdate = ps.executeUpdate();
                System.out.println("Exito al actualizar");
            } else {
                System.out.println("Error al actualizar");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Error al desconectar");
            }
        }
        return executeUpdate;
    }
    
    
    public ArrayList<Coordinador> listarDb() {
        String sql;
        PreparedStatement ps;
        ArrayList<Coordinador> coordinadores = new ArrayList<>();
        Proyecto pro;
        try {
            ResultSet rs;
            Coordinador coor;
            if (this.conectar()) {
                sql = "SELECT coordinador.*,proyecto.nombre FROM coordinador INNER JOIN proyecto ON coordinador.codigoProyecto=proyecto.codigoProyecto";
                ps = this.getConn().prepareCall(sql);

                rs = ps.executeQuery();
                while (rs.next()) {
                    
                    pro=new Proyecto();
                    pro.setCodigoProyecto(rs.getInt(5));
                    pro.setNombre(rs.getString(9));
                    
                    coor = new Coordinador();
                    coor.setCodigoCoordinador(rs.getInt(1));
                    coor.setNombre(rs.getString(2));
                    coor.setEdad(rs.getInt(3));
                    coor.setSueldoBase(rs.getDouble(4));
                    coor.setProyecto(pro);
                    coor.setIntereses(UtilidadesVarias.separarString(rs.getString(6),","));
                    coor.setSexo(rs.getString(7));
                    coor.setArea(rs.getString(8));
                    coordinadores.add(coor);
                }
                rs.close();
                ps.close();
            } else {

                System.out.println("Error");
            }
        } catch (SQLException e) {
            coordinadores = null;
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return coordinadores;
    }    
    
    
    public Coordinador buscarOjetoDb(Coordinador coordinador) {
        String sql;
        PreparedStatement ps;
        Coordinador coor = null;
        ResultSet rs;
        Proyecto pro;
        try {
            if (this.conectar()) {
                sql = "SELECT coordinador.*,proyecto.nombre FROM coordinador INNER JOIN proyecto ON coordinador.codigoProyecto=proyecto.codigoProyecto WHERE codigoCoordinador=?";
                ps = this.getConn().prepareCall(sql);
                ps.setInt(1, coordinador.getCodigoCoordinador());
                rs = ps.executeQuery();

                rs.next();
                    pro=new Proyecto();
                    pro.setCodigoProyecto(rs.getInt(5));
                    pro.setNombre(rs.getString(9));
                    
                    coor = new Coordinador();
                    coor.setCodigoCoordinador(rs.getInt(1));
                    coor.setNombre(rs.getString(2));
                    coor.setEdad(rs.getInt(3));
                    coor.setSueldoBase(rs.getDouble(4));
                    coor.setProyecto(pro);
                    coor.setIntereses(UtilidadesVarias.separarString(rs.getString(6),","));
                    coor.setSexo(rs.getString(7));
                    coor.setArea(rs.getString(8));

                rs.close();
                ps.close();
            } else {
                System.out.println("Error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (this.desconectar()) {
                System.out.println("Desconectado");
            } else {
                System.out.println("Conectado aun");
            }
        }
        return coor;
    }    
}

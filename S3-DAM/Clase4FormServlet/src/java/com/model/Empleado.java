/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author RobertoPC
 */
public class Empleado {
    String codigo;
    String nombre;
    String sexo;
    String departamento;
    double salario;
    String[] intereses;

    public Empleado() {
    }

    public Empleado(String codigo, String nombre, String sexo, String departamento, double salario, String[] intereses) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.sexo = sexo;
        this.departamento = departamento;
        this.salario = salario;
        this.intereses = intereses;
    }
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String[] getIntereses() {
        return intereses;
    }

    public void setIntereses(String[] intereses) {
        this.intereses = intereses;
    }
    
    
}

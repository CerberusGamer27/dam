<%-- 
    Document   : resultado
    Created on : 08-13-2022, 12:36:32 PM
    Author     : RobertoPC
--%>
<%@page import="com.model.Empleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    Empleado emp=(Empleado)request.getSession().getAttribute("empleado");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    </head>
    <body>
        <div class="container mt-3">
        <h1>Datos!</h1>
        Código: <%= emp.getCodigo() %> <br>
        Nombre: <%= emp.getNombre()%>  <br>
        Sexo: <%= emp.getSexo()%> <br>
        Departamento: <%= emp.getDepartamento()%> <br>
        Salario: <%= emp.getSalario()%> <br>
        Intereses: <% 
                for(int i=0; i<emp.getIntereses().length; i++)
                {
                    out.write("<b>"+emp.getIntereses()[i]+"</b><br>");
                }
        
        %> 
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </body>
</html>

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.clases;

/**
 * Class  Name: Persona
 * Date: 7 oct. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Estudiante {
    public double notaDam;
    public String sexo;
    public String nombres;

    public Estudiante() {
    }

    public Estudiante(double notaDam, String sexo, String nombres) {
        this.notaDam = notaDam;
        this.sexo = sexo;
        this.nombres = nombres;
    }

    public double getNotaDam() {
        return notaDam;
    }

    public void setNotaDam(double notaDam) {
        this.notaDam = notaDam;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
       
}

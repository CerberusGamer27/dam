<%-- 
    Document   : index
    Created on : 7 oct. 2022, 19:15:39
    Author     : C3rberus
--%>

<%@page import="com.clases.Estudiante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
    <head>
        <title>JSTL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body>

        <nav class="navbar bg-light ">
            <div class="container-fluid ">
                <a class="navbar-brand " href="#">
                    <img src="https://www.svgrepo.com/show/14636/jsp-open-file-format-with-java-logo.svg" alt="" width="35" height="29" class="d-inline-block align-text-top">
                    Ejemplo JSTL 
                </a>
            </div>
        </nav>
        <div class="container">
            <h1 class="display-5 ">JSTL Core (JSP Standard TAG Library) </h1>

            <!--<a href="index.html">Regresar Al Index</a><br>-->

            <%
                Estudiante estudiante1 = new Estudiante(9.2, "Masculino", "Jose Perez");
                Estudiante estudiante2 = new Estudiante(7.1, "Masculino", "Juan Martinez");
                Estudiante estudiante3 = new Estudiante(9.9, "Femenino", "Estefany Garcia");
                Estudiante estudiantes[] = {estudiante1, estudiante2, estudiante3};
                request.setAttribute("estudiantes", estudiantes);
            %>

            <ul>
                <c:forEach var="estudiante" items="${estudiantes}">
                    <li>Nombres: ${estudiante.nombres} || Nota DAM:  ${estudiante.notaDam}</li>
                    </c:forEach>
            </ul>
            <br>
            <c:forEach var="estudiante" items="${estudiantes}">
                <c:set var = "sumaNota" value = "${sumaNota + estudiante.notaDam}"/>
            </c:forEach>

            <c:forEach var="estudiante" items="${estudiantes}">
                <c:if test = "${estudiante.sexo=='Femenino'}">
                    <c:set var = "numeroMujeres" value = "${numeroMujeres + 1}"/>
                </c:if>
            </c:forEach>

            <c:forEach var="estudiante" items="${estudiantes}" varStatus="loop">
                <c:choose>
                    <c:when test="${loop.index == 0}">
                        <c:set var = "notaMayor" value = "${estudiante.notaDam}"/>
                    </c:when>
                    <c:when test="${estudiante.notaDam > notaMayor}">
                        <c:set var = "notaMayor" value = "${estudiante.notaDam}"/>
                    </c:when>
                    <c:otherwise>
                    </c:otherwise>
                </c:choose>
            </c:forEach>


            <c:set var = "promedioNota" value = "${sumaNota/3}"/>


            <p>Promedio Nota: ${promedioNota}</p>
            <p>Numero Mujeres: ${numeroMujeres}</p>
            <p>Nota Mayor: ${notaMayor}</p>
            <br>
        </div>


        <footer class="text-center pb-5">
            <hr class="container">  
            <span class="mb-3 mb-md-0 text-muted">ITCA-FEPADE � 2022 Roberto Armijo - 046719</span>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    </body>
</html>

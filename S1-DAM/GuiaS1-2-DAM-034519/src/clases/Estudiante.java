/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author C3rberus
 */
public class Estudiante {
    public int carnet;
    public String nombre, apellidos;
    public double cum;
    public String grupo;

    public Estudiante() {
    }

    public Estudiante(int carnet, String nombre, String apellidos, double cum, String grupo) {
        this.carnet = carnet;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.cum = cum;
        this.grupo = grupo;
    }

    public int getCarnet() {
        return carnet;
    }

    public void setCarnet(int carnet) {
        this.carnet = carnet;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public double getCum() {
        return cum;
    }

    public void setCum(double cum) {
        this.cum = cum;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
    
    public void calcularEstado(){
        System.out.println("Estado");
    }
}

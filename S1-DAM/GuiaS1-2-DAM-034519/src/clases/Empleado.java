package clases;

import java.util.Date;

/**
 *
 * @author C3rberus
 */
public class Empleado {
    String nombre, direccion;
    int codigoEmpleado, edad;
    Area area;
    double sueldoBase;
    Date fechaIngreso;

    public Empleado() {
    }

    public Empleado(String nombre, String direccion, int codigoEmpleado, int edad, Area area, double sueldoBase, Date fechaIngreso) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.codigoEmpleado = codigoEmpleado;
        this.edad = edad;
        this.area = area;
        this.sueldoBase = sueldoBase;
        this.fechaIngreso = fechaIngreso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCodigoEmpleado() {
        return codigoEmpleado;
    }

    public void setCodigoEmpleado(int codigoEmpleado) {
        this.codigoEmpleado = codigoEmpleado;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public double getSueldoBase() {
        return sueldoBase;
    }

    public void setSueldoBase(double sueldoBase) {
        this.sueldoBase = sueldoBase;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombre=" + nombre + ", direccion=" + direccion + ", codigoEmpleado=" + codigoEmpleado + ", edad=" + edad + ", area=" + area.getNombreArea() + ", sueldoBase=" + sueldoBase + ", fechaIngreso=" + fechaIngreso + '}';
    }
    
    
}

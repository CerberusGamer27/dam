/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author C3rberus
 */
public class EstudianteComputacion extends Estudiante {
    public String carrera, genero;
    int carnet, anioNacimiento;
    double cum, renta;

    public EstudianteComputacion() {
        super();
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getCarnet() {
        return carnet;
    }

    public void setCarnet(int carnet) {
        this.carnet = carnet;
    }

    public int getAnioNacimiento() {
        return anioNacimiento;
    }

    public void setAnioNacimiento(int anioNacimiento) {
        this.anioNacimiento = anioNacimiento;
    }

    public double getCum() {
        return cum;
    }

    public void setCum(double cum) {
        this.cum = cum;
    }

    public double getRenta() {
        return renta;
    }

    public void setRenta(double renta) {
        this.renta = renta;
    }
    
    
    
    public void calcularEdad(){
        int anioActual = Calendar.getInstance().get(Calendar.YEAR);
        int edad = anioActual - this.anioNacimiento;
        JOptionPane.showMessageDialog(null, "La edad del alumno es: "+edad);
    }
    
    public void calcularEstado(){
        if(this.cum > 7){
            JOptionPane.showMessageDialog(null, "Estado: APROBADO");
        } else {
            JOptionPane.showMessageDialog(null, "Estado: REPROBADO");
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EstudianteComputacion{");
        sb.append("carrera=").append(carrera);
        sb.append(", genero=").append(genero);
        sb.append(", carnet=").append(carnet);
        sb.append(", anioNacimiento=").append(anioNacimiento);
        sb.append(", cum=").append(cum);
        sb.append('}');
        return sb.toString();
    }
    
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author C3rberus
 */
public class MetodoPago {
    public String tipoPago, nombreCliente;
    public double montoCompra;
    public double DESC = 0.40, RECARGO = 0.10;

    public MetodoPago(String tipoPago, String tipoCliente, double montoCompra) {
        this.tipoPago = tipoPago;
        this.nombreCliente = tipoCliente;
        this.montoCompra = montoCompra;
    }

    public MetodoPago() {
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public double getMontoCompra() {
        return montoCompra;
    }

    public void setMontoCompra(double montoCompra) {
        this.montoCompra = montoCompra;
    }
    
    public double totalPagar(){
        if(this.getTipoPago().equals("PUBLICO")){
            return this.getMontoCompra() - (this.getMontoCompra() * DESC);
        } else{
            return this.getMontoCompra() + (this.getMontoCompra() * RECARGO);
        }
    }
    
    
}

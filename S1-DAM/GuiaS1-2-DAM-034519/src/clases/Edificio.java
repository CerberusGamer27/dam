/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author C3rberus
 */
public class Edificio {
    String ubicacion, nombre;
    int cantidadDepartamentos, cantidadPisos;

    public Edificio() {
    }

    public Edificio(String ubicacion, String nombre, int cantidadDepartamentos, int cantidadPisos) {
        this.ubicacion = ubicacion;
        this.nombre = nombre;
        this.cantidadDepartamentos = cantidadDepartamentos;
        this.cantidadPisos = cantidadPisos;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadDepartamentos() {
        return cantidadDepartamentos;
    }

    public void setCantidadDepartamentos(int cantidadDepartamentos) {
        this.cantidadDepartamentos = cantidadDepartamentos;
    }

    public int getCantidadPisos() {
        return cantidadPisos;
    }

    public void setCantidadPisos(int cantidadPisos) {
        this.cantidadPisos = cantidadPisos;
    }
    
    
}

package clases;

/**
 *
 * @author C3rberus
 */
public class Area {
    int codigoArea, cantidadEmpleados;
    String nombreArea;
    Edificio edificio;

    public Area() {
    }

    public Area(int codigoArea, int cantidadEmpleados, String nombreArea, Edificio edificio) {
        this.codigoArea = codigoArea;
        this.cantidadEmpleados = cantidadEmpleados;
        this.nombreArea = nombreArea;
        this.edificio = edificio;
    }

    public int getCodigoArea() {
        return codigoArea;
    }

    public void setCodigoArea(int codigoArea) {
        this.codigoArea = codigoArea;
    }

    public int getCantidadEmpleados() {
        return cantidadEmpleados;
    }

    public void setCantidadEmpleados(int cantidadEmpleados) {
        this.cantidadEmpleados = cantidadEmpleados;
    }

    public String getNombreArea() {
        return nombreArea;
    }

    public void setNombreArea(String nombreArea) {
        this.nombreArea = nombreArea;
    }

    public Edificio getEdificio() {
        return edificio;
    }

    public void setEdificio(Edificio edificio) {
        this.edificio = edificio;
    }
    
    
}

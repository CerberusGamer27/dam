/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas;

import clases.EstudianteComputacion;
import javax.swing.JOptionPane;

/**
 *
 * @author C3rberus
 */
public class SistemaNotas {
    public static void main(String[] args) {
        EstudianteComputacion estudianteComputacion = new EstudianteComputacion();
        
        // Carnet alumno
        while (true) {
            try {
                int carnet = Integer.parseInt(JOptionPane.showInputDialog("Ingrese carnet del alumno"));
                estudianteComputacion.setCarnet(carnet);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un carnet valido");
                continue;
            }
        }
        
        // Input Datos
        String nombres = JOptionPane.showInputDialog("Ingrese los nombres del alumno");
        String apellidos = JOptionPane.showInputDialog("Ingrese los apellidos del alumno");
        String carrera = JOptionPane.showInputDialog("Ingrese la carrera del alumno");
        
        // Set datos Alumno
        estudianteComputacion.setNombre(nombres);
        estudianteComputacion.setApellidos(apellidos);
        estudianteComputacion.setCarrera(carrera);
        
        // Option Dialog Genero
        int seleccion = JOptionPane.showOptionDialog(null, "Sistema de Estudiante",
                "Ingrese el genero del alumno", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,
                new Object[]{"MASCULINO","FEMENINO"},"opcion 1");
        
        if (seleccion == 0) {
            estudianteComputacion.setGenero("MASCULINO");
        } else {
            estudianteComputacion.setGenero("FEMENINO");
        }
        
        // anioNacimineto del Alumno
        while (true) {
            try {
                int anio = Integer.parseInt(JOptionPane.showInputDialog("Ingrese anio de nacimiento del alumno"));
                estudianteComputacion.setAnioNacimiento(anio);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un anio valido valido");
                continue;
            }
        }
        
        // C.U.M del Alumno
        while (true) {
            try {
                double cum = Double.parseDouble(JOptionPane.showInputDialog("Ingrese C.U.M del alumno"));
                if (cum >=0 && cum<=10) {
                    estudianteComputacion.setCum(cum);
                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese un C.U.M valido");
                    continue;
                }
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un valor correcto");
                continue;
            }
        }
        
        // Renta del Alumno
        while (true) {
            try {
                double renta = Double.parseDouble(JOptionPane.showInputDialog("Ingrese la renta del alumno"));
                if (renta > 0) {
                    estudianteComputacion.setRenta(renta);
                } else {
                    continue;
                }
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un valor correcto");
                continue;
            }
        }
        
        //Grupo del alumno
        String grupo = JOptionPane.showInputDialog("Ingrese el grupo del alumno");
        estudianteComputacion.setGrupo(grupo);
        
        estudianteComputacion.calcularEdad();
        estudianteComputacion.calcularEstado();
        
        
        
        
    }
}

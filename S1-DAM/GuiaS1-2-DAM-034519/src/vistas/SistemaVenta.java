/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas;

import clases.MetodoPago;
import javax.swing.JOptionPane;

/**
 *
 * @author C3rberus
 */
public class SistemaVenta {
    public static void main(String[] args) {
        int seleccion = JOptionPane.showOptionDialog(null, "Sistema de Venta a Mayoristas",
                "Selector de opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,
                new Object[]{"PUBLICO","PRIVADO"},"opcion 1");
        
        if(seleccion == 0){
            String nombreCliente = JOptionPane.showInputDialog("Ingrese el nombre del cliente");
            if(nombreCliente == null || (nombreCliente != null && ("".equals(nombreCliente))))   
            {
                JOptionPane.showMessageDialog(null, "Usted Cancelo la Operacion");
            }
            String compra = JOptionPane.showInputDialog("Ingrese el valor de la compra");
            if(compra == null || (compra != null && ("".equals(compra))))   
            {
                JOptionPane.showMessageDialog(null, "Usted Cancelo la Operacion");
            }
            double montoCompra = Double.parseDouble(compra);
            MetodoPago metodoPago = new MetodoPago();
            metodoPago.setNombreCliente(nombreCliente);
            metodoPago.setMontoCompra(montoCompra);
            metodoPago.setTipoPago("PUBLICO");
            JOptionPane.showMessageDialog(null, "El total a pagar es: $"+metodoPago.totalPagar());
            
        } else if(seleccion == 1){
            String nombreCliente = JOptionPane.showInputDialog("Ingrese el nombre del cliente");
            if(nombreCliente == null || (nombreCliente != null && ("".equals(nombreCliente))))   
            {
                JOptionPane.showMessageDialog(null, "Usted Cancelo la Operacion");
            }
            String compra = JOptionPane.showInputDialog("Ingrese el valor de la compra");
            if(compra == null || (compra != null && ("".equals(compra))))   
            {
                JOptionPane.showMessageDialog(null, "Usted Cancelo la Operacion");
            }
            double montoCompra = Double.parseDouble(compra);
            MetodoPago metodoPago = new MetodoPago();
            metodoPago.setNombreCliente(nombreCliente);
            metodoPago.setMontoCompra(montoCompra);
            metodoPago.setTipoPago("PRIVADO");
            JOptionPane.showMessageDialog(null, "El total a pagar es: $"+metodoPago.totalPagar());
        }
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas;

import javax.swing.JOptionPane;

/**
 *
 * @author C3rberus
 */
public class Vectores {
    public static void main(String[] args) {
        // Declaracion
        int[] numerosPositivos = new int[10];
        int[] numerosNegativos = new int[10];
        
        // Asignacion
        numerosPositivos[0] = 5;
        numerosPositivos[1] = 41;
        numerosPositivos[2] = 321;
        numerosPositivos[3] = 21;
        numerosPositivos[4] = 6;
        numerosPositivos[5] = 9;
        numerosPositivos[6] = 5;
        numerosPositivos[7] = 4;
        numerosPositivos[8] = 961;
        numerosPositivos[9] = 214;
        
        numerosNegativos[0] = -5;
        numerosNegativos[1] = -41;
        numerosNegativos[2] = -321;
        numerosNegativos[3] = -21;
        numerosNegativos[4] = -6;
        numerosNegativos[5] = -9;
        numerosNegativos[6] = -5;
        numerosNegativos[7] = -4;
        numerosNegativos[8] = -961;
        numerosNegativos[9] = -214;
        
        int[] vectorProbar = new int[10];
        
        int positivos = 0;
        int negativos = 0;
        for (int i = 0; i < 10; i++) {
            
            if (numerosNegativos[i] >= 0) {
                positivos++;
            }else {
                negativos++;
            }
        }
        if(positivos == 10){
            JOptionPane.showMessageDialog(null, "VERDADERO");
        } else if(negativos == 10){
            JOptionPane.showMessageDialog(null, "FALSO");
        } else{
            JOptionPane.showMessageDialog(null, "MIXTO");
        }
    }
       
}

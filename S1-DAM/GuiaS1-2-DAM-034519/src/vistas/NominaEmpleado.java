/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author C3rberus
 */
public class NominaEmpleado {
    public static void main(String[] args) {
        List<Double> empleados =new ArrayList<Double>();
        try {
            int numeroEmpleados = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero de empleados"));
            for (int i = 0; i < numeroEmpleados; i++) {
                while (true) {                   
                    try{
                        double test = Double.parseDouble(JOptionPane.showInputDialog("Ingrese el salario del empleado "+(i+1)));
                        empleados.add(test);
                        break;
                    } catch (Exception e){
                        JOptionPane.showMessageDialog(null, "Ingrese un valor correcto");
                        continue;
                    }
                }
            }
            int mayor1500 = 0;
            int entre = 0 ;
            int menos850 = 0;
            String nomina = Arrays.toString(empleados.toArray());
            JOptionPane.showMessageDialog(null, "Nomina de Empleados \n\n"+nomina);
            for (Double empleado : empleados) {
                if (empleado > 1500) {
                    mayor1500++;
                } else if(empleado >= 850 && empleado <= 1500){
                    entre++;
                } else {
                    menos850++;
                }
            }
            String conteo = "Numero Empleados que ganan arriba de $1500: " + mayor1500 + "\n\n"
                            + "Numero Empleados que ganan entre $850 y $1500: " + entre + "\n\n"
                            + "Numero Empleados con salario menor  a $850: " + menos850 + "\n\n";
            JOptionPane.showMessageDialog(null, """
                                                Conteo de empleados por salario
                                                    -------------------------------
                                                """
                                                + conteo);
            // (double) en las operaciones se utiliza ya que, al no especificar el tipo con el que se esta trabajando JAVA asume que son enteros
            
            String porcentaje = "Porcentaje Empleados que ganan arriba de $1500: %" + (((double)mayor1500/(double)numeroEmpleados)*100) + "\n\n"
                            + "Empleados que ganan entre $850 y $1500: %" + (((double)entre/(double)numeroEmpleados)*100) + "\n\n"
                            + "Empleados con salario menor  a $850: %" + (((double)menos850/(double)numeroEmpleados)*100) + "\n\n";
            
            JOptionPane.showMessageDialog(null, """
                                                Porcentaje de empleados por salario
                                                    -------------------------------
                                                """
                                                + porcentaje);
            
        } catch (Exception e) {
            System.out.println("Error");
        }
    }
}

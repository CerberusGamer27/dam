package vistas;

import clases.Area;
import clases.Edificio;
import clases.Empleado;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author C3rberus
 */
public class SistemaITCA {
    public static void main(String[] args) {
        Empleado empleado = new Empleado();
        Edificio edificio = new Edificio("Ala Norte", "Edicicio F", 1, 5);
        Area area = new Area(1, 10, "Informatica", edificio);
        
        // Nombre
        empleado.setNombre(JOptionPane.showInputDialog("Ingrese el nombre del empleado"));
        
        // Codigo
        while (true) {
            try {
                int codEmpleado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el codigo del empleado"));
                empleado.setCodigoEmpleado(codEmpleado);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un valor valido");
                continue;
            }
        }
        
        // Edad
        while (true) {
            try {
                int edad = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la edad del empleado"));
                empleado.setEdad(edad);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un valor valido");
                continue;
            }
        }
        
        // Direccion
        empleado.setDireccion(JOptionPane.showInputDialog("Ingrese la direccion del empleado"));
        
        // Area
        empleado.setArea(area);
        
        // Sueldo Base
        while (true) {
            try {
                double sueldoBase = Double.parseDouble(JOptionPane.showInputDialog("Ingrese el sueldo base del empleado"));
                empleado.setSueldoBase(sueldoBase);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un valor valido");
                continue;
            }
        }
        
        // Fecha
        while (true) {
            try {
                String fechaIngreso = JOptionPane.showInputDialog("Ingrese la fecha de ingreso del Empleado dd/MM/yyyy");
                Date date =new SimpleDateFormat("dd/MM/yyyy").parse(fechaIngreso);  
                empleado.setFechaIngreso(date);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese un valor valido");
                continue;
            }
        }
        
        JOptionPane.showMessageDialog(null, "Datos del Empleado \n\n\n\n" + empleado.toString());
    }
}

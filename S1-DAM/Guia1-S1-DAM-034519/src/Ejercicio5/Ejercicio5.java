package Ejercicio5;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio5 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio5 {

    public static void main(String[] args) {
        double max = 0, prom = 0, sum = 0;
        double MIN = 99999999;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de Empleados");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] salario = new double[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("Ingrese el nombre del empleado");
            nombre[i] = leer.next();
            System.out.println("Ingrese el salario: $");
            salario[i] = leer.nextDouble();
            System.out.println("\n");
            if (salario[i] > max) {
                max = salario[i];
            }
            if (salario[i] < MIN) {
                MIN = salario[i];
            }
            sum += salario[i];
        }
        prom = sum / n;
        System.out.println("\n\n\n");
        System.out.println("La suma de los salarios es: $" + sum);
        System.out.println("El salario mayor es: $" + max);
        System.out.println("El salario menor es: $" + MIN);
        System.out.println("El promedio de los salarios es: $" + prom);

    }
}

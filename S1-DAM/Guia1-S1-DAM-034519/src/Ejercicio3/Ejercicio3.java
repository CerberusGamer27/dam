package Ejercicio3;

import java.util.Scanner;

/**
 * Nombre de la clase: 
 * Ejercicio3 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio3 {

    public static void main(String[] args) {

        String estado = "";
        double a = 0, r = 0, p = 0, s = 0, f = 0;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de alumnos");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] nota = new double[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("Ingrese el nombre del estudiante");
            nombre[i] = leer.next();
            System.out.println("Ingrese la nota:");
            nota[i] = leer.nextDouble();
            if (nota[i] > 6.89) {
                nota[i] = 7;
            }

            if ((nota[i] >= 6.5) && (nota[i] <= 6.89)) {
                estado = "Reprobó con derecho a examen de suficiencia";
                a++;
            } else if (nota[i] >= 7) {
                estado = "Aprobo la asignatura";
                r++;
            } else if (nota[i] < 6.5) {
                estado = "Reprobo sin derecho a examen de suficiencia";
                f++;
            }
            s += nota[i];
        }

        p = s / n;
        System.out.println("Alumnos Reprobados con derecho a examen de suficiencia: " + a);
        System.out.println("Alumnos Reprobados sin derecho a examen de suficiencia: " + f);
        System.out.println("Alumnos Aprobados de la asignatura " + r);
        System.out.println("Promedio de notas: " + p);
    }
}

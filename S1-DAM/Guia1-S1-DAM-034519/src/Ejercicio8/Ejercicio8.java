package Ejercicio8;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio8 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio8 {

    public static void main(String[] args) {
        double max = 0, credito = 0, comision = 0, sum =0;
        double min = 99999999;
        int n, anios = 0, porcentaje = 0;
        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de Empleados");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] salario = new double[n];
        int[] tipo = new int[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("Ingrese el nombre del empleado");
            nombre[i] = leer.next();
            System.out.println("Ingrese el salario: $");
            salario[i] = leer.nextDouble();
            System.out.println("Ingrese el tipo de empleado (1 = privado y 2 = publico):");
            tipo[i] = leer.nextInt();

            if (tipo[i] == 1) {
                credito = salario[i] * 10;
                credito = credito;
                comision = credito * 0.05;
                anios = 7;
                porcentaje = 5;
            } else if (tipo[i] == 2) {
                credito = salario[i] * 7;
                credito = credito;
                comision = credito * 0.03;
                anios = 5;
                porcentaje = 3;
            }
            sum += comision;
            if (credito > max) {
                max = credito;
            } else if (credito < min) {
                min = credito;
            }

            System.out.println("\n");
            System.out.println("El empleado " + nombre[i] + " Obtendra un monto de credito de: $" + credito);
            System.out.println("Por un plazo de " + anios + " años y la comision sera del " + 5 + "%");
            System.out.println("\n");
        }
        System.out.println("\n");
        System.out.println("El credito mayor es: $" + max);
        System.out.println("El credito menor es: $" + min);
        System.out.println("El total de comision es: $" + sum);

    }
}

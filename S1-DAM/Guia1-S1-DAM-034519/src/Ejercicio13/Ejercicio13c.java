package Ejercicio13;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio13c 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio13c {

    public static void main(String[] args) {

        int n;
        Scanner leer = new Scanner(System.in);
        double peso = 0, altura = 0, sum = 0, prom = 0, max = 0;
        double MIN = 999999999;
        System.out.print("Ingrese la cantidad de datos: ");
        n = leer.nextInt();

        for (int i = 1; i <= n; i++) {
            System.out.print("Ingrese el peso en kg: ");
            peso = leer.nextDouble();
            System.out.print("Ingrese la estatura cm: ");
            altura = leer.nextDouble();
            System.out.println("\n");

            sum += peso;

            if (altura > max) {
                max = altura;

            }
            if (peso < MIN) {
                MIN = peso;
            }
        }

        prom = sum / n;
        System.out.println("\n");
        System.out.println("El peso promedio es de: " + prom + "kg");
        System.out.println("La estatura mayor es de: " + max + "cm");
        System.out.println("El peso menor es de: " + MIN + "kg");

    }
}

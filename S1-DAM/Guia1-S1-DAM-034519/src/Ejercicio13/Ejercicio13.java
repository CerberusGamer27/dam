package Ejercicio13;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio13 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio13 {

    public static void main(String[] args) {
        String nombre;
        int genero = 0, labora = 0;
        double sueldo = 0;
        double estatura = 0;
        double peso = 0;
        double pm = 0, ph = 0, pm2 = 0, ph2 = 0;
        int no = 0, si = 0;
        double sum = 0, prom = 0, prom2 = 0, sum2 = 0;
        int e = 0;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de personas a entrevistar ");
        n = leer.nextInt();
        int numeros[] = new int[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("\n");
            System.out.println("Encuesta");
            System.out.print("Ingrese el nombre: ");
            nombre = leer.next();
            System.out.println("Ingrese el Genero");
            System.out.println("1 = Masculino");
            System.out.println("2 = Femenino");
            genero = leer.nextInt();
            System.out.print("Ingrese su Peso en kg: ");
            peso = leer.nextDouble();
            System.out.print("Ingrese su estatura en cm: ");
            estatura = leer.nextDouble();
            System.out.println("Trabaja: ");
            System.out.println("1 = Si");
            System.out.println("2 = no");
            labora = leer.nextInt();

            if (labora == 1) {
                System.out.print("Ingrese su salario: $");
                sueldo = leer.nextDouble();
                System.out.println("\n");
                si++;
            } else {
                sueldo = 0;
                no++;
            }

            if (genero == 2) {
                pm++;
                if (sueldo == 0) {
                    pm2++;
                }
            } else {
                ph++;
                if (sueldo > 0) {
                    ph2++;
                }
            }

            if ((sueldo >= 0) && (sueldo <= 500)) {
                sum2 += sueldo;
                e++;
            }
            sum += sueldo;
        }
        pm = pm * 1 / n;
        ph = ph * 1 / n;
        pm2 = pm2 * 1 / no;
        ph2 = ph2 * 1 / si;
        prom = sum / n;
        prom2 = sum2 / e;

        System.out.println("\n");
        System.out.println("El porcentaje de hombres es: " + ph + "%" + " Y el porcentaje de mujeres es de: " + pm + "%");
        System.out.println("El porcentaje de hombres que trabajan es de: " + ph2 + "%");
        System.out.println("El porcentaje de mujeres que no trabajan es de: " + pm2 + "%");
        System.out.println("El sueldo promedio de todos los entrevistados es de: $" + prom);
        System.out.println("El sueldo promedio de los que poseen un salario entre $0.00 y $500.00 es: $" + prom2);

    }
}

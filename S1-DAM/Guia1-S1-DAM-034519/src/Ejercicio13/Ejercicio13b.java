package Ejercicio13;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio13b 
 * Fecha 26/07/22 
 * Version 1.0
 * @author C3rberus
 */
public class Ejercicio13b {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        System.out.print("Introduzca un número entero: ");
        int n = leer.nextInt();
        leer.close();

        System.out.println();
        for (int l = 1; l <= n; l++) {
            //Espacios en blanco
            for (int b = 1; b <= n - l; b++) {
                System.out.print(" ");
            }

            //Asteriscos
            for (int a = 1; a <= (l * 2) - 1; a++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}

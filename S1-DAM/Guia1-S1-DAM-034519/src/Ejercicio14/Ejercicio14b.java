package Ejercicio14;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio14b 
 * Fecha 27/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio14b {

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int matriz[][] = new int[3][3];
        int dato = 1;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print("Ingrese un numero en la posición " + i + "," + j + ": ");
                matriz[i][j] = leer.nextInt();
            }
        }
        System.out.println("\n");
        System.out.println("La Matriz es: ");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print("\t" + matriz[i][j]);
            }
            System.out.println("");
        }

        int diagonalPrincipal[] = new int[matriz.length];
        int diagonalSecundaria[] = new int[matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (i == j) {
                    diagonalPrincipal[i] = matriz[i][j];
                }
                if ((i + j) == matriz.length - 1) {
                    diagonalSecundaria[i] = matriz[i][j];
                }
            }
        }

        int suma = 0;
        System.out.println("\nDiagonal principal");
        for (int elemento : diagonalPrincipal) {
            System.out.print("\t" + elemento);
            suma = suma + elemento;
        }
        System.out.print(" = " + suma);
        System.out.println("");

        suma = 0;
        System.out.println("\nDiagonal secundaria");
        for (int elemento : diagonalSecundaria) {
            System.out.print("\t" + elemento);
            suma = suma + elemento;
        }
        System.out.print(" = " + suma);
        System.out.println("");

    }
}

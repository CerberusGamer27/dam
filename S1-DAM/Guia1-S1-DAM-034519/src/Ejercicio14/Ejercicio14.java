package Ejercicio14;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio14 
 * Fecha 27/07/22 
 * version 1.0
 * @author Cerberus
 */
public class Ejercicio14 {

    public static void main(String[] args) {
        int matriz[][] = new int[3][3];
        Scanner leer = new Scanner(System.in);
        int sum = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                System.out.print("Ingrese un numero en la posición " + i + "," + j + ": ");
                matriz[i][j] = leer.nextInt();
                sum += matriz[i][j];
            }
        }

        if (sum == 0) {
            System.out.println("La Matriz es nula");
        } else {
            System.out.println("La Matriz es no es nula");
        }
    }
}

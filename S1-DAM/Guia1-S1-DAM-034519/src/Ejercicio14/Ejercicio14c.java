package Ejercicio14;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio14c 
 * Fecha 27/07/22 
 * Version 1.0
 * @author C3rberus
 */
public class Ejercicio14c {

    public static void main(String[] args) {
        String estado = "";
        double matriz[][] = new double[7][7];
        Scanner leer = new Scanner(System.in);
        int sum = 0, sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0, sum5 = 0, sum6 = 0;
        double min = 999999;
        double max = 0, prom = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (i == 0) {
                    estado = "Lunes";
                } else if (i == 1) {
                    estado = "Martes";
                } else if (i == 2) {
                    estado = "Miercoles";
                } else if (i == 3) {
                    estado = "Jueves";
                } else if (i == 4) {
                    estado = "Viernes";
                } else if (i == 5) {
                    estado = "Sabado";
                } else if (i == 6) {
                    estado = "Domingo";
                }

                System.out.print("Ingrese el monto ventas realizadas el día " + estado + " en la sucursal #" + (j + 1) + " $");
                matriz[i][j] = leer.nextDouble();

                if (matriz[i][j] > max) {
                    max = matriz[i][j];
                } else if (matriz[i][j] < min) {
                    min = matriz[i][j];
                }

                switch (estado) {
                    case "Lunes":
                        sum += matriz[0][j];
                        break;
                    case "Martes":
                        sum1 += matriz[1][j];
                        break;
                    case "Miercoles":
                        sum2 += matriz[i][j];
                        break;
                    case "Jueves":
                        sum3 += matriz[i][j];
                        break;
                    case "Viernes":
                        sum4 += matriz[i][j];
                        break;
                    case "Sabado":
                        sum5 += matriz[i][j];
                        break;
                    case "Domingo":
                        sum6 += matriz[i][j];
                        break;
                }
            }
            System.out.println("");
        }

        for (int i = 0; i <= 6; i++) {
            if (i == 0) {
                prom = sum / 7;
            } else if (i == 1) {
                prom = sum1 / 7;
            } else if (i == 2) {
                prom = sum2 / 7;
            } else if (i == 3) {
                prom = sum3 / 7;
            } else if (i == 4) {
                prom = sum4 / 7;
            } else if (i == 5) {
                prom = sum5 / 7;
            } else if (i == 6) {
                prom = sum6 / 7;
            }

            System.out.println("El promedio de venta de la sucursal #" + (i + 1) + " es de: $" + prom);
        }

        System.out.println("El total de ventas del dia Miercoles fue de: $" + sum2);
        System.out.println("La venta mayor fue de: $" + max);
        System.out.println("La venta menor fue de: $" + min);
    }
}


package Ejercicio2;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio2
 * Fecha 26/07/22
 * version 1.0
 * @author C3rberus 
 */
public class Ejercicio2 {
    public static void main(String[] args) {
        
          String estado="";
          double a=0, r=0, p=0, s=0;
          int n;
          Scanner leer = new Scanner(System.in);
          
          System.out.println("Ingrese la cantidad de alumnos");
          n = leer.nextInt();
          int numeros[] = new int[n];
          String[] nombre = new String[n];
          double[] nota = new double[n];
          
          for(int i=0; i<numeros.length; i++){
              System.out.println("Ingrese el nombre del estudiante");
              nombre[i] = leer.next();
              System.out.println("Ingrese el CUM del estudiante");
              nota[i] = leer.nextDouble();
              if(nota[i] >= 7){
              estado = "Egresado";
              a++;
              }
              else{
                  estado = "No egresado";
                  r++;
              }
              s += nota[i];
          }
           
          p = s/n;
          System.out.println("\n\n\nAlumnos Egresados: " + a);
          System.out.println("Alumnos No egresados: " + r);
          System.out.println("Promedio de CUM: " + p);
    }
}

package Ejercicio7;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio7 
 * Fecha 26/07/22
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio7 {

    public static void main(String[] args) {
        double max = 0;
        double MIN = 99999999;
        double desc = 0, sumdesc = 0, total;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de Clientes");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] pago = new double[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("\n");
            System.out.println("Compra");
            System.out.println("Ingrese el nombre del cliente");
            nombre[i] = leer.next();
            System.out.println("Ingrese el monto a pagar: $");
            pago[i] = leer.nextDouble();
            System.out.println("\n");
            if (pago[i] < 100) {
                desc = pago[i] * 0.1;
                total = pago[i] - desc;
                sumdesc += desc;
            } else {
                total = pago[i];
            }

            if (total > max) {
                max = total;
            } else if (total < MIN) {
                MIN = total;
            }
            System.out.println("El monto a pagar el cliente: " + nombre[i] + " Es: $" + total);
        }
        System.out.println("\n");
        System.out.println("El monto total por descuentos es de: $" + sumdesc);
        System.out.println("El monto a pagar mayor es: $" + max);
        System.out.println("El monto a pagar menor es: $" + MIN);
    }
}

package Ejercicio10;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio10 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio10 {

    public static void main(String[] args) {
        double sumaDes = 0, desc = 0;
        String estado = "";
        int elantra = 0;
        double total = 0, suma = 0;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de Clientes");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] precio = new double[n];
        int modelo;

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("\n");
            System.out.println("Compra");
            System.out.print("Ingrese el nombre del cliente");
            nombre[i] = leer.next();
            System.out.print("Ingrese el precio del vehiculo: $");
            precio[i] = leer.nextDouble();
            System.out.println("Ingrese el modelo");
            System.out.println("1 si es el modelo Sprak");
            System.out.println("2 si es el modelo Elantra");
            System.out.println("3 si es el modelo Accent");
            modelo = leer.nextInt();
            System.out.println("\n");

            switch (modelo) {
                case 1:
                    desc = precio[i] * 0.12;
                    total = precio[i] - desc;
                    estado = "12%";
                    break;
                case 2:
                    desc = precio[i] * 0.08;
                    total = precio[i] - desc;
                    estado = "8%";
                    elantra++;
                    break;
                case 3:
                    desc = precio[i] * 0.10;
                    total = precio[i] - desc;
                    estado = "10%";
                    break;
                default:
                    System.out.println("Ingrese una categoria valida: ");
                    break;
            }
            suma += total;
            sumaDes += desc;
            System.out.println("\n");
            System.out.println("El cliente " + nombre[i] + " Se le aplico un descuento del: " + estado);
            System.out.println("El precio inicial era de: $" + precio[i]);
            System.out.println("El precio Final con descuento es de: $" + total);
        }
        System.out.println("\n");
        System.out.println("El total vendido es de: $" + suma);
        System.out.println("El total en descuentos es de $" + sumaDes);
        System.out.println("El modelo Elantra tuvo un total de descuentos de: " + elantra);
        System.out.println("El total de compras es de: " + n + " compras");

    }
}

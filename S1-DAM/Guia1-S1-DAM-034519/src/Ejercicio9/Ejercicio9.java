package Ejercicio9;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio9 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio9 {

    public static void main(String[] args) {
        double recar = 0.1, desc = 0.4;
        String estado = "";
        double total = 0;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de Clientes: ");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] pago = new double[n];
        int[] tipo = new int[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("\n");
            System.out.println("Compra");
            System.out.print("Ingrese el nombre del cliente");
            nombre[i] = leer.next();
            System.out.print("Ingrese el monto a pagar");
            pago[i] = leer.nextDouble();
            System.out.print("Ingrese el tipo de pago (1 = publico y 2 = privado)");
            tipo[i] = leer.nextInt();
            System.out.println("\n");

            if (tipo[i] == 1) {
                total = pago[i] * desc;
                total = pago[i] - total;
                estado = "Descuento";

            } else if (tipo[i] == 2) {
                total = pago[i] * recar;
                total = pago[i] + total;
                estado = "Recargo";
            }

            System.out.println("\n");
            System.out.println("El cliente " + nombre[i] + " Se le aplico un " + estado + ", su total a pagar sera de: $" + total);
        }

    }
}

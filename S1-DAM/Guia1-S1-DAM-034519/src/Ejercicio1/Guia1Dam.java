
package guia1dam;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: ejerciciosGuia1
 * Fecha 26/07/22
 * version 1.0
 * @author C3rberus
 */
public class Guia1Dam {

    public static void main(String[] args) {
        String[] nombre = new String[5];
          double[] nota = new double[5];
          String estado="";
          double a=0, r=0, p=0, s=0;
          Scanner leer = new Scanner(System.in);
          
          for(int i=0; i<5; i++){
              System.out.println("Ingrese el nombre del estudiante");
              nombre[i] = leer.next();
              System.out.println("Ingrese la nota del estudiante");
              nota[i] = leer.nextDouble();
              if(nota[i] >= 7){
              estado = "Aprobado";
              a++;
              }
              else{
                  estado = "Reprobado";
                  r++;
              }
              s += nota[i];
          }
           
          p = s/5;
          System.out.println("Alumnos Aprobados: " + a);
          System.out.println("Alumnos Reprobados: " + r);
          System.out.println("Promedio de notas: " + p);
    }
    
}

package Ejercicio6;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio6 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio6 {

    public static void main(String[] args) {
        double max = 0, prom = 0, sum = 0;
        double MIN = 99999999;
        double dispone, indice;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de personas a entrevistar");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] salario = new double[n];
        double[] gastos = new double[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("\n");
            System.out.println("Entrevista " + (i + 1));
            System.out.println("Ingrese el nombre: ");
            nombre[i] = leer.next();
            System.out.println("¿Cuál es su salario mensual? $");
            salario[i] = leer.nextDouble();
            System.out.println("¿Cuál es el total de sus gastos mensuales? $");
            gastos[i] = leer.nextDouble();

            dispone = salario[i] - gastos[i];
            indice = dispone / salario[i];

            sum += dispone;

            if (dispone > max) {
                max = dispone;
            } else if (dispone < MIN) {
                MIN = dispone;
            }
            System.out.println("La cantidad de dinero que puede ahorrar " + nombre[i] + " es: $" + dispone);
            System.out.println("Su indice de ahorro es: " + indice + "%");
            System.out.println("\n");
        }
        prom = sum / n;
        System.out.println("\n");
        System.out.println("El Ahorro mayor es: $" + max);
        System.out.println("El Ahorro menor es: $" + MIN);
        System.out.println("El promedio de Ahorro es: $" + prom);

    }
}

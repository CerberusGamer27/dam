package Ejercicio4;

import java.util.Scanner;

/**
 * Nombre de la clase: Ejercicio4 
 * Fecha 26/07/22 
 * version 1.0
 * @author C3rberus
 */
public class Ejercicio4 {

    public static void main(String[] args) {
        double salarioFinal, bono;
        int n;
        Scanner leer = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de Empleados");
        n = leer.nextInt();
        int numeros[] = new int[n];
        String[] nombre = new String[n];
        double[] sueldo = new double[n];
        int[] categoria = new int[n];

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("Ingrese el nombre del empleado");
            nombre[i] = leer.next();
            System.out.println("Ingrese el sueldo: $");
            sueldo[i] = leer.nextDouble();
            System.out.println("Ingrese la categoria (1, 2 o 3):");
            categoria[i] = leer.nextInt();

            if (categoria[i] == 1) {
                System.out.println("\n");
                System.out.println("El empleado " + nombre[i] + " Obtendra un bono del 60%");
                bono = sueldo[i] * 0.6;
                salarioFinal = sueldo[i] + bono;
                System.out.println("El salario Final es: $" + salarioFinal);
                System.out.println("\n");
            } else if (categoria[i] == 2) {
                System.out.println("\n");
                System.out.println("El empleado " + nombre[i] + " Obtendra un bono del 45%");
                bono = sueldo[i] * 0.45;
                salarioFinal = sueldo[i] + bono;
                System.out.println("El salario Final es: $" + salarioFinal);
                System.out.println("\n");
            } else if (categoria[i] == 3) {
                System.out.println("\n");
                System.out.println("El empleado " + nombre[i] + " Obtendra un bono del 30%");
                bono = sueldo[i] * 0.3;
                salarioFinal = sueldo[i] + bono;
                System.out.println("El salario Final es: $" + salarioFinal);
                System.out.println("\n");
            } else {
                System.out.println("\n");
                System.out.println("El empleado " + nombre[i] + " No obtendra bono");
                salarioFinal = sueldo[i];
                System.out.println("El salario Final es: $" + salarioFinal);
                System.out.println("\n");
            }
        }

    }
}

<%-- 
    Document   : resultado
    Created on : 30 jul. 2022, 19:24:44
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            String email = "";
            String password = "";
            String[] intereses = {""};
            if (request.getParameter("btnEnviar") != null) {
                email = request.getParameter("txtEmail");
                password = request.getParameter("txtPassword");
                intereses = request.getParameterValues("check");
            }
        %>
        <div class="container">
            <div class="row justify-content-center mt-5 pt-5">
                <div class="col-4">
                    <label class="text-danger">Email: <strong><%= email%></strong></label>
                    <label class="text-danger">Password: <strong><%= password%></strong></label>
                    <label class="text-danger">Intereses: <strong><%
                            for (int i = 0; i < intereses.length; i++) {
                                    out.write("<strong>" + intereses[i] + "</strong>");
                                }
                        %>
                        </strong></label>
                </div>
            </div>
        </div>
    </body>
</html>

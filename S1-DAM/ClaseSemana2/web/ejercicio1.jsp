<%-- 
    Document   : ejercicio1
    Created on : 30 jul. 2022, 14:43:20
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="column">
                    <form action="resultado.jsp" method="post">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" class="form-control" name="txtEmail" id="txtEmail" aria-describedby="emailHelp">
                            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Password</label>
                            <input type="password" name="txtPassword" id="txtPassword" class="form-control" id="txtPassword">
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" name="check" value="valor1" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Valor 1</label>
                        </div>
                        <div class="mb-3 form-check">
                            <input class="form-check-input" name="check" type="checkbox" value="valor2" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                Valor 2
                            </label>
                        </div>
                        <div class="mb-3 form-check">
                            <input class="form-check-input" name="check" type="checkbox" value="valor3" id="flexCheckChecked">
                            <label class="form-check-label" for="flexCheckChecked">
                                Valor 3
                            </label>
                        </div>
                        <button type="submit" name="btnEnviar" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    </body>
</html>

<%-- 
    Document   : footer
    Created on : 26 sep. 2022, 21:25:30
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<footer class="footer mt-auto py-3">
    <div class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top container">
        <div class="col-md-4 d-flex align-items-center">
        <a href="#" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
            GG
        </a>
        <span class="text-muted">&copy; 2022 Company, Inc</span>
    </div>

    <ul class="nav col-md-7 justify-content-end list-unstyled d-flex">
        <li class="nav-item me-3"><a href="#" class="nav-link active">ID Sesion: ${cookie.JSESSIONID.value} </a></li>
        <li class="nav-item me-3"><a href="Otro.jsp" class="nav-link active">Link EJ1 </a></li>
        <li class="nav-item"><a href="#" class="nav-link active">Link EJ2 </a></li>
    </ul>
    </div>
</footer>


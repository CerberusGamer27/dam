package com.controllers;

import com.dao.DaoSupplier;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.model.Supplier;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author RobertoPC
 */
@WebServlet(name = "SupplierController", urlPatterns = {"/supplierController"})
public class SupplierController extends HttpServlet {

    private final DaoSupplier daoS = new DaoSupplier();
    private final Supplier su = new Supplier();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        RequestDispatcher rd = null;
        try {
            //Seteando valores
            su.setId(Integer.parseInt(request.getParameter("codigoSupplier")));
            su.setName(request.getParameter("txtName"));
            su.setAddress(request.getParameter("txtAddress"));
            su.setPhone(request.getParameter("txtPhone"));
            
            if (request.getParameter("btnAdd") != null) {
                daoS.addSupplier(su);
            } else if (request.getParameter("btnEdit") != null) {
                daoS.updateSupplier(su);
            } else if (request.getParameter("btnDelete") != null) {
                daoS.deleteSupplier(su);
            }
            rd = request.getRequestDispatcher("index.jsp");
        } catch (Exception e) {
        }
        rd.forward(request, response);
         
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

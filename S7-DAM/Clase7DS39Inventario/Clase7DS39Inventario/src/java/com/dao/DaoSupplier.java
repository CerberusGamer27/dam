/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.conexion.Conexion;
import com.model.Supplier;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author RobertoPC
 */
public class DaoSupplier extends Conexion{
    
    public List<Supplier> getSupplier() throws SQLException {
        ResultSet rs;
        Supplier su;
        List<Supplier> lst = new ArrayList();
        try {
            this.conectar();
            String sql = "select * from suppliers";
            PreparedStatement pst = this.getCon().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                su = new Supplier(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
                lst.add(su);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            this.desconectar();
        }
        return lst;
    }
    
    public void addSupplier(Supplier su) throws SQLException{
        try {
            this.conectar();
            String sql = "insert into suppliers (name,phone,address) values (?,?,?);";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, su.getName());
            pre.setString(2, su.getAddress());
            pre.setString(3, su.getPhone());
            pre.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            this.desconectar();
        }
    }
    
    public void updateSupplier(Supplier su) throws SQLException{
        try {
            this.conectar();
            String sql = "update suppliers set name = ?, address = ?, phone = ? where id = ?;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, su.getName());
            pre.setString(2, su.getAddress());
            pre.setString(3, su.getPhone());
            pre.setInt(4, su.getId());
            pre.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            this.desconectar();
        }
    }
    
     public void deleteSupplier(Supplier su) throws SQLException {
        try {
            this.conectar();
            String sql = "delete from suppliers where id = ?;";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, su.getId());
            pre.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            this.desconectar();
        }
    }
    
}

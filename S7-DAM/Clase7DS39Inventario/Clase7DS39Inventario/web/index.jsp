

<%@page import="java.util.List"%>
<%@page import="com.dao.DaoSupplier"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.model.Supplier" %>
<!doctype html>
<%
    DaoSupplier daoS = new DaoSupplier();
%>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
        <title>Proveedores</title>
    </head>
    <body>


        <div class="row mt-5 justify-content-center">
            <div class="col-11">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div class="col-auto"><h1>Proveedores</h1></div>
                            <div class="col-12 col-xl-auto">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <form action="supplierController" method="POST" name="frmSupplier" id="frmSup">
                <input type="hidden" value="1" name="codigoSupplier"/>
                <!--<div class="mb-3">
                    <label for="codigo" class="col-form-label">Código</label>
                    <input type="text" class="form-control" id="codigo" name="txtCodigo">
                </div>-->
                <div class="mb-3">
                    <label for="name" class="col-form-label">Nombre:</label>
                    <input type="text" class="form-control" id="name" name="txtName">
                </div>
                <div class="mb-3">
                    <label for="message-text" class="col-form-label">Dirección:</label>
                    <textarea class="form-control" id="message-text" name="txtAddress"></textarea>
                </div>
                <div class="mb-3">
                    <label for="phone" class="col-form-label">Teléfono:</label>
                    <input type="text" class="form-control" id="phone" name="txtPhone">
                </div>
                <div class="mb-3">
                    <input type="submit" value="Insertar" name="btnAdd" />
                    <input type="submit" value="Actualizar" name="btnEdit" />
                    <input type="submit" value="Eliminar" name="btnDelete" />
                </div>
            </form>
        </div>

        <!-- Optional JavaScript; choose one of the two! -->

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        -->
    </body>
</html>
<script>
    function cargar(idS, nombre, direccion, telefono)
    {
        document.frmSupplier.codigoSupplier.value = idS;
        document.frmSupplier.txtName.value = nombre;
        document.frmSupplier.txtAddress.value = direccion;
        document.frmSupplier.txtPhone.value = telefono;
    }
    ;
</script>

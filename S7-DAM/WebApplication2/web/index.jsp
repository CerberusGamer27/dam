
<%@page import="com.clases.Persona"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@page import="java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
    <head>
        <title>JSTL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body>

        <nav class="navbar bg-light ">
            <div class="container-fluid ">
                <a class="navbar-brand " href="#">
                    <img src="https://www.svgrepo.com/show/14636/jsp-open-file-format-with-java-logo.svg" alt="" width="35" height="29" class="d-inline-block align-text-top">
                    Ejemplo JSTL 
                </a>
            </div>
        </nav>
        <div class="container">
            <h1 class="display-5 ">JSTL Core (JSP Standard TAG Library) </h1>

            <!--<a href="index.html">Regresar Al Index</a><br>-->

            <%
                Persona persona1 = new Persona(162, "Masculino", "Jose Perez");
                Persona persona2 = new Persona(175, "Femenino", "Juana Maria");
                Persona persona3 = new Persona(178, "Masculino", "Juan Perez");
                Persona personas[] = {persona1, persona2, persona3};
                request.setAttribute("personas", personas);
            %>

            <ul>
                <c:forEach var="persona" items="${personas}">
                    <li>Nombres: ${persona.nombres} || Estatura:  ${persona.estatura}</li>
                    </c:forEach>
            </ul>
            <br>
            <c:forEach var="persona" items="${personas}">
                <c:set var = "sumaEstatura" value = "${sumaEstatura + persona.estatura}"/>
            </c:forEach>

            <c:set var = "promedioEstatura" value = "${sumaEstatura/3}"/>

            <%
                double prom = Double.valueOf(request.getAttribute("promedioEstatura").toString());
                DecimalFormat priceFormatter = new DecimalFormat("$#0.00");
                request.setAttribute("promedioFormato", priceFormatter.format(prom));
            %>
            
            <p>${promedioEstatura}</p>
            <p>${promedioFormato}</p>
            <br>
        </div>


        <footer class="text-center pb-5">
            <hr class="container">  
            <span class="mb-3 mb-md-0 text-muted">ITCA-FEPADE � 2022 Roberto Armijo - 046719</span>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    </body>
</html>



<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>JSTL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body>

        <nav class="navbar bg-light ">
            <div class="container-fluid ">
                <a class="navbar-brand " href="#">
                    <img src="https://www.svgrepo.com/show/14636/jsp-open-file-format-with-java-logo.svg" alt="" width="35" height="29" class="d-inline-block align-text-top">
                    Ejemplo JSTL 
                </a>
            </div>
        </nav>
        <div class="container">
            <h1 class="display-5 ">JSTL Core (JSP Standard TAG Library) </h1>
            <!--VAriable -->
            <c:set var="nombre" value="Pedro" />
            <!--MOstrar vairalbe -->
            valor de la variable: <c:out value="${nombre}" />
            <br>
            <c:out value="<h3>DS39</h2>" escapeXml="false" />
            <c:set var="bandera" value="true" />
            <c:if test="${bandera}">
                La bandera es Verdadera
            </c:if>
                <br>

            <c:if test="${param.opcion!=1}">
                <c:choose>
                    <c:when test="${param.opcion==1}">
                        valor 1 seleccionado
                    </c:when>
                    <c:when test="${param.opcion==2}">
                        valor 2 seleccionado
                    </c:when>
                    <c:otherwise>
                        Opcion no valida: ${param.opcion}
                    </c:otherwise>
                </c:choose>
            </c:if>

            <%
                String nombres[] = {"Peras", "Manzanas", "Naranjas"};
                request.setAttribute("nombres", nombres);

            %>
            <br>
            <ul>
                <c:forEach var="frutas" items="${nombres}">
                    <li>${frutas}</li>
                </c:forEach>
            </ul>

            <a href="index.html">Regresar Al Index</a><br>
        </div>


        <footer class="text-center pb-5">
            <hr class="container">  
            <span class="mb-3 mb-md-0 text-muted">ITCA-FEPADE � 2022 Roberto Armijo - 046719</span>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    </body>
</html>

<%-- 
    Document   : completar
    Created on : 26 ago. 2022
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 2</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    </head>
    <body>
        <div class="container mt-5">
            <%
                boolean logeado = false;
                Cookie[] cookies = request.getCookies();

                //Verificar si esta logeado
                if (cookies != null) {
                    for (Cookie c : cookies) {
                        if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                            //Si existe la cookie, el visitante ya esta logeado
                            logeado = true;
                            break;
                        }
                    }
                }
                if (logeado) {
            %>
            <div class="row">
                <div class="col-md-5">
                    <h1 class="text-center">Regresar al Index</h1>
                    <a href="index.jsp" class="text-center"><i class="bi bi-chevron-left"></i> Regresar a la lista</a>  
                </div>
            </div>
            <%
                    } else {
                        response.sendError(response.SC_FORBIDDEN, "Credenciales Incorrectas");
                    }
            %>
        </div>
    </body>
</html>

<%-- 
    Document   : resultado
    Created on : 26 ago. 2022
    Author     : C3rberus
--%>

<%@page import="com.models.Empleado"%>
<%@page import="com.utils.Utilidades"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    Empleado emp=(Empleado)request.getSession().getAttribute("empleado");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 2</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="mt-5">Datos Procesados</h4>
            <br>
            <a href="inicio.jsp" class="text-center"><i class="bi bi-chevron-left"></i> Regresar a la lista</a>  
            <main>
                <div class="row mt-5">
                <dl class="row border">
                    <dt class="col-sm-4">
                        Codigo:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getCodigo()%>
                    </dd>
                    <dt class="col-sm-4">
                        Nombre:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getNombre()%>
                    </dd>
                    <dt class="col-sm-4">
                        Año Ingreso:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getAnioIngreso()%>
                    </dd>
                    <dt class="col-sm-4">
                        Edad Empresa (Años):
                    </dt>
                    <dd class="col-sm-8">
                        <%= Utilidades.tiempoEmpresa(emp.getAnioIngreso())%>
                    </dd>
                    <dt class="col-sm-4">
                        Edad:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getEdad()%>
                    </dd>
                    <dt class="col-sm-4">
                        Salario Bruto:
                    </dt>
                    <dd class="col-sm-8">
                        $<%= emp.getSueldo()%>
                    </dd>
                    <dt class="col-sm-4">
                        Salario Neto:
                    </dt>
                    <dd class="col-sm-8">
                        $<%= Utilidades.calcularSalario(emp.getSueldo())%>
                    </dd>
                    <dt class="col-sm-4">
                        Genero:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getSexo()%>
                    </dd>
                    <dt class="col-sm-4">
                        Departamento:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getDepartamento()%>
                    </dd>
                    <dt class="col-sm-4">
                        Cargo:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getCargo()%>
                    </dd>
                    <dt class="col-sm-4">
                        Edad:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getEdad()%>
                    </dd>
                    <dt class="col-sm-4">
                        Capacitaciones
                    </dt>
                    <dd class="col-sm-8">
                        <ul class="list-group">
                            <%
                                for (int i = 0; i < emp.getCapacitaciones().length; i++) {
                                    out.write("<li class='list-group-item'>"+emp.getCapacitaciones()[i]+"</li>");
                                }
                            %>
                        </ul>
                    </dd>
                    <dt class="col-sm-4">
                        Telefono:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getTelefono()%>
                    </dd>
                    <dt class="col-sm-4">
                        DUI:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getDui()%>
                    </dd>
                    <dt class="col-sm-4">
                        NIT:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getNit()%>
                    </dd>
                    <dt class="col-sm-4">
                        Direccion:
                    </dt>
                    <dd class="col-sm-8">
                        <%= emp.getDireccion()%>
                    </dd>
                </dl>
            </div>
            </main>
        </div>
    </body>
</html>

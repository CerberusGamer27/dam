<%-- 
    Document   : ejercicio2
    Created on : 26 ago. 2022
    Author     : C3rberus
--%>

<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 2</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>
        <div class="container py-4">
            <%
                boolean logeado = false;
                Cookie[] cookies = request.getCookies();

                //Verificar si esta logeado
                if (cookies != null) {
                    for (Cookie c : cookies) {
                        if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                            //Si existe la cookie, el visitante ya esta logeado
                            logeado = true;
                            break;
                        }
                    }
                }
                if (logeado) {
            %>
            <header class="pb-3 mb-4 border-bottom">
                <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
                    <span class="fs-1">Ejercicio 2 DAM</span>
                </a>
            </header>
            <a href="inicio.jsp" class="text-center mt-5"><i class="bi bi-chevron-left"></i> Regresar a la lista</a>  
            <h4>Formulario Completo</h4>
            <br>

            <h3 class="text-sm-center">Datos Personales</h3>
            <form id="formularioTrabajo" action="procesarEmpleado" method="post">
                <div class="row g-3">
                    <div class="col-sm-2">
                        <label for="txtCodigo" class="form-label">Código</label>
                        <input type="text" class="form-control" id="txtCodigo" name="txtCodigo" placeholder="Tu código aqui" required>
                        <div class="invalid-feedback">
                            Código es requerido.
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="txtNombre" class="form-label">Nombres</label>
                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Tu Nombre aqui" required>
                        <div class="invalid-feedback">
                            Nombre es requerido.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtAnio" class="form-label">Año de ingreso</label>
                        <input type="number" class="form-control" id="txtAnio" name="txtAnio" required 
                               title = "Ingrese un año menor al año actual">
                        <div class="invalid-feedback">
                            Año de ingreso es requerido.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtEdad" class="form-label">Edad</label>
                        <input type="number" class="form-control" id="txtEdad" name="txtEdad" placeholder="18" required
                               min = "0" title = "Ingrese una edad valida">
                        <div class="invalid-feedback">
                            Edad es requerida.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtSueldo" class="form-label">Sueldo $</label>
                        <input type="number" class="form-control" id="txtSueldo" name="txtSueldo" placeholder="$" required 
                               min = "0" title = "Ingrese un sueldo valido">
                        <div class="invalid-feedback">
                            Sueldo es requerido.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="sexo" class="form-label">Sexo</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="masculino" value="Masculino" required="">
                            <label class="form-check-label" for="masculino">
                                Masculino
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="femenino" value="Femenino" required="">
                            <label class="form-check-label" for="femenino">
                                Femenino
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="cmbDepartamento" class="form-label">Departamento</label>
                        <select class="form-select" id="cmbDepartamento" name="cmbDepartamento" aria-label="" required>
                            <option selected disabled value="">Seleccione un Departamento</option>
                            <option value = "Finanzas" >Departamento Finanzas</option>
                            <option value = "Informatica" >Departamento de Informatica</option>
                            <option value = "Ventas" >Departamento de Ventas</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <label for="cmbCargo" class="form-label">Cargo</label>
                        <select class="form-select" id="cmbCargo" name="cmbCargo" aria-label="" required>
                            <option selected disabled value>Seleccione un Cargo</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label class="form-label">Capacitaciones</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="SCRUM" name="capacitaciones" id="cScrum">
                            <label class="form-check-label" for="cScrum">
                                SCRUM
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="AWS" name="capacitaciones" id="cAws">
                            <label class="form-check-label" for="cAws">
                                AWS
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Azure" name="capacitaciones" id="cAzure">
                            <label class="form-check-label" for="cAzure">
                                Azure
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Google Cloud" name="capacitaciones" id="cGcp">
                            <label class="form-check-label" for="cGcp">
                                Google Cloud
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Ingles" name="capacitaciones" id="cIngles">
                            <label class="form-check-label" for="cIngles">
                                Ingles
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="telefono" class="form-label">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" required title="Ingrese un telefono en formato ####-####"
                               placeholder="7010-0010" pattern="[0-9]{4}-[0-9]{4}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="dui" class="form-label">DUI</label>
                        <input type="text" class="form-control" id="dui" name="dui" required title="Ingrese un DUI con guiones"
                               placeholder="01020304-0" pattern="[0-9]{8}-[0-9]{1}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="dui" class="form-label">NIT</label>
                        <input type="text" class="form-control" id="txtDui" name="nit" required title="Ingrese un NIT con guiones"
                               placeholder="123-010100-123-1" pattern="[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label for="direccion" class="form-label">Direccion</label>
                        <textarea class="form-control" id="direccion" name="direccion" rows="2" required=""
                                  placeholder="123 St.John Av"></textarea>
                        <div class="invalid-feedback">
                        </div>
                    </div>


                    <div class="text-center">
                        <button class="w-70 btn btn-outline-primary btn-sm" name="btnEnviar" type="submit">Envíar</button>
                    </div>

                </div>
            </form>
            <% } else {
                    response.sendError(response.SC_FORBIDDEN, "Credenciales Incorrectas");
                }
            %>

        </div>
        <script>
                    // Selectores
                    const cmbDepartamento = document.querySelector('#cmbDepartamento');
                    const cmbCargo = document.querySelector('#cmbCargo');

                    // Cargos
                    const cargosFinanzas = ['Contador/a', 'Auxiliar Contable', 'Auditor/a'];
                    const cargosInformatica = ['Programador/a', 'Analista', 'DBA'];
                    const cargosVentas = ['Vendedor/a', 'Cajero/a', 'Supervisor/a'];

                    //Eventos
                    cmbDepartamento.addEventListener('change', (e) => {
                        console.log(e.target.value);
                        console.log(cmbCargo);
                        while (cmbCargo.options.length > 0) {
                            cmbCargo.remove(0);
                        }
                        switch (e.target.value) {
                            case "Finanzas":
                                cargosFinanzas.forEach(cargo => {
                                    const opt = document.createElement("option");
                                    opt.value = cargo;
                                    opt.text = cargo;
                                    cmbCargo.add(opt, null);
                                });
                                break;
                            case "Informatica":
                                cargosInformatica.forEach(cargo => {
                                    const opt = document.createElement("option");
                                    opt.value = cargo;
                                    opt.text = cargo;
                                    cmbCargo.add(opt, null);
                                });
                                break;
                            case "Ventas":
                                cargosVentas.forEach(cargo => {
                                    const opt = document.createElement("option");
                                    opt.value = cargo;
                                    opt.text = cargo;
                                    cmbCargo.add(opt, null);
                                });
                                break;
                            default:
                                break;
                        }

                    });
                    document.addEventListener('DOMContentLoaded', () => {
                        const anioIngreso = document.querySelector("#txtAnio");
                        anioIngreso.max = new Date().getFullYear();
                        anioIngreso.placeholder = new Date().getFullYear();
                    });
        </script>
    </body>
</html>
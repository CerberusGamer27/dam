<%-- 
    Document   : inicio
    Created on : 26 ago. 2022
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 2</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>
    <body>
        <main>
            <div class="container py-4">
                <%
                    boolean logeado = false;
                    Cookie[] cookies = request.getCookies();

                    //Verificar si esta logeado
                    if (cookies != null) {
                        for (Cookie c : cookies) {
                            if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                                //Si existe la cookie, el visitante ya esta logeado
                                logeado = true;
                                break;
                            }
                        }
                    }
                    if (logeado) {
                %>
                <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
                    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                        <i class="bi bi-app-indicator"></i>
                        <span class="fs-4">Simple header</span>
                    </a>

                    <ul class="nav nav-pills">
                        <li class="nav-item"><a href="index.html" class="nav-link">Salir</a></li>
                        <li class="nav-item"><a href="excel.jsp" class="nav-link">DESCARGAR CREDENCIALES</a></li>
                    </ul>
                </header>
                <div class="row align-items-md-stretch">
                    <div class="col-md-6">
                        <div class="h-100 p-5 text-white bg-dark rounded-3">
                            <h2>Ejercicio 1 Formulario</h2>
                            <p>Formulario con Scriptlets</p>
                            <a class="btn btn-outline-light" href="ejercicio1.jsp">Ejercicio 1</a>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="h-100 p-5 bg-dark text-white border rounded-3">
                            <h2>Ejercicio 2 Formulario</h2>
                            <p>Formulario con Servlets</p>
                            <a class="btn btn-outline-light" href="ejercicio2.jsp">Ejercicio 2</a>
                        </div>
                    </div>
                </div>
                <%
                    } else {
                        response.sendError(response.SC_FORBIDDEN, "Credenciales Incorrectas");
                    }
                %>

            </div>
        </main>
    </body>
</html>

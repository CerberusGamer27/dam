package com.utils;

import java.util.Calendar;

/**
 * Class  Name: Utilidades
 * Date: 26 ago. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Utilidades {
public static double calcularSalario(double salario) {
        double ISSS = 0.07;
        double ISR = 0.10;
        double FSV = 0.075;
        double AFP = 0.0725;
        double salarioFinal = 0;
        salarioFinal = salario - (salario * ISSS) - (salario * ISR) - (salario * FSV) - (salario * AFP);
        return salarioFinal;
    }

    public static int tiempoEmpresa(int entrada) {
        Calendar cal = Calendar.getInstance();
        int anio = cal.get(Calendar.YEAR) - entrada;
        return anio;
    }
}

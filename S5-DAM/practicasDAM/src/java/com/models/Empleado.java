package com.models;

/**
 * Class  Name: Empleado
 * Date: 26 ago. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Empleado {
    String codigo;
    String nombre;
    int anioIngreso;
    int edad;
    double sueldo;
    String sexo;
    String departamento;
    String cargo;
    String[] capacitaciones;
    String telefono;
    String dui;
    String nit;
    String direccion;

    public Empleado() {
    }

    public Empleado(String codigo, String nombre, int anioIngreso, int edad, double sueldo, String sexo, String departamento, String cargo, String[] capacitaciones, String telefono, String dui, String nit, String direccion) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.anioIngreso = anioIngreso;
        this.edad = edad;
        this.sueldo = sueldo;
        this.sexo = sexo;
        this.departamento = departamento;
        this.cargo = cargo;
        this.capacitaciones = capacitaciones;
        this.telefono = telefono;
        this.dui = dui;
        this.nit = nit;
        this.direccion = direccion;
    }

    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnioIngreso() {
        return anioIngreso;
    }

    public void setAnioIngreso(int anioIngreso) {
        this.anioIngreso = anioIngreso;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String[] getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(String[] capacitaciones) {
        this.capacitaciones = capacitaciones;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }
    
    
}

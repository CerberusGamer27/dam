package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class Name: Empleado Date: 26/08/2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class loginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            // Cookies
            boolean logeado = false;
            Cookie[] cookies = request.getCookies();

            // Valores
            String user = "admin";
            String password = "admin";
            String txtUser = request.getParameter("txtUsername");
            String txtPassword = request.getParameter("txtPassword");

            if (user.equals(txtUser) && password.equals(txtPassword)) {
                Cookie visitanteN = new Cookie("logeado", "si");
                response.addCookie(visitanteN);
                response.sendRedirect("inicio.jsp");
            } else {
                response.sendError(response.SC_FORBIDDEN, "Credenciales Incorrectas");
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        //Logeado
        boolean logeado = false;
        Cookie[] cookies = request.getCookies();


        //Verificar si esta logeado
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                    //Si existe la cookie, el visitante ya esta logeado
                    logeado = true;
                    break;
                }
            }
        }
        if (logeado) {
            response.sendRedirect("inicio.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

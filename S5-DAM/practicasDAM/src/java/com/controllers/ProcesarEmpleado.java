package com.controllers;

import com.models.Empleado;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class Name: Empleado Date: 26/08/2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
public class ProcesarEmpleado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        
        boolean logeado = false;
        Cookie[] cookies = request.getCookies();

        //Verificar si esta logeado
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                    //Si existe la cookie, el visitante ya esta logeado
                    logeado = true;
                    break;
                }
            }
        }
        if (logeado) {
            Empleado emp = new Empleado();
            try ( PrintWriter out = response.getWriter()) {
                if (request.getParameter("btnEnviar") != null) {
                    emp.setCodigo(request.getParameter("txtCodigo"));
                    emp.setNombre(request.getParameter("txtNombre"));
                    emp.setAnioIngreso(Integer.parseInt(request.getParameter("txtAnio")));
                    emp.setEdad(Integer.parseInt(request.getParameter("txtEdad")));
                    emp.setSueldo(Double.parseDouble(request.getParameter("txtSueldo")));
                    emp.setSexo(request.getParameter("rGenero"));
                    emp.setDepartamento(request.getParameter("cmbDepartamento"));
                    emp.setCargo(request.getParameter("cmbCargo"));
                    emp.setCapacitaciones(request.getParameterValues("capacitaciones"));
                    emp.setTelefono(request.getParameter("telefono"));
                    emp.setDui(request.getParameter("dui"));
                    emp.setNit(request.getParameter("nit"));
                    emp.setDireccion(request.getParameter("direccion"));
                    request.getSession().setAttribute("empleado", emp);
                    response.sendRedirect("resultado.jsp");
                } else {
                    response.sendRedirect("completar.jsp");
                }
            } catch (Exception e) {
                response.sendRedirect("completar.jsp");
            }
        } else {
            response.sendError(response.SC_FORBIDDEN, "Credenciales Incorrectas");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

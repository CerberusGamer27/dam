/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.models;

/**
 * Class  Name: Usuario
 * Date: 26 ago. 2022
 * Version: 1.0
 * Copyright: Free
 * @author Geovanny Martinez (034519)
 */
public class Usuario {
    String dui;
    String nombre;
    String Apellidos;
    int anioNacimiento;
    double sueldo;
    String genero;
    String categoria;
    String especialidad;
    String[] interes;
    String telefono;
    String afp;
    String isss;
    String direccion;

    public Usuario() {
    }

    public Usuario(String dui, String nombre, String Apellidos, int anioNacimiento, double sueldo, String genero, String categoria, String especialidad, String[] interes, String telefono, String afp, String isss, String direccion) {
        this.dui = dui;
        this.nombre = nombre;
        this.Apellidos = Apellidos;
        this.anioNacimiento = anioNacimiento;
        this.sueldo = sueldo;
        this.genero = genero;
        this.categoria = categoria;
        this.especialidad = especialidad;
        this.interes = interes;
        this.telefono = telefono;
        this.afp = afp;
        this.isss = isss;
        this.direccion = direccion;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public int getAnioNacimiento() {
        return anioNacimiento;
    }

    public void setAnioNacimiento(int anioNacimiento) {
        this.anioNacimiento = anioNacimiento;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String[] getInteres() {
        return interes;
    }

    public void setInteres(String[] interes) {
        this.interes = interes;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getAfp() {
        return afp;
    }

    public void setAfp(String afp) {
        this.afp = afp;
    }

    public String getIsss() {
        return isss;
    }

    public void setIsss(String isss) {
        this.isss = isss;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}

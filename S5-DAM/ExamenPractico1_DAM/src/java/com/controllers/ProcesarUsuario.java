/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package com.controllers;

import com.models.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class  Name: ProcesarUsuario
 * Date: 26 ago. 2022
 * Version: 1.0
 * CopyRight: Free
 * @author Geovanny Martinez(034519)
 */
public class ProcesarUsuario extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         boolean logeado = false;
        Cookie[] cookies = request.getCookies();

        //Verificar si esta logeado
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                    //Si existe la cookie, el visitante ya esta logeado
                    logeado = true;
                    break;
                }
            }
        }
        if (logeado) {
            Usuario us = new Usuario();
            try ( PrintWriter out = response.getWriter()) {
                if (request.getParameter("btnEnviar") != null) {
                    us.setDui(request.getParameter("dui"));
                    us.setNombre(request.getParameter("txtNombre"));
                    us.setApellidos(request.getParameter("txtApellidos"));
                    us.setAnioNacimiento(Integer.parseInt(request.getParameter("txtAnio")));
                    us.setSueldo(Double.parseDouble(request.getParameter("txtSueldo")));
                    if(request.getParameter("txtOtro") != null){
                        us.setGenero(request.getParameter("txtOtro"));  
                    } else {
                        us.setGenero(request.getParameter("rGenero"));
                    }
                    us.setCategoria(request.getParameter("cmbCategoria"));
                    us.setEspecialidad(request.getParameter("cmbEspecialidad"));
                    us.setInteres(request.getParameterValues("intereses"));
                    us.setTelefono(request.getParameter("telefono"));
                    us.setAfp(request.getParameter("afp"));
                    us.setIsss(request.getParameter("isss"));
                    us.setDireccion(request.getParameter("direccion"));
                    request.getSession().setAttribute("usuario", us);
                    response.sendRedirect("resultado.jsp");
                } else {
                    response.sendError(response.SC_PRECONDITION_FAILED, "Faltan Valores");
                }
            } catch (Exception e) {
                response.sendError(response.SC_PRECONDITION_FAILED, "Faltan Valores");
            }
        } else {
            response.sendError(response.SC_FORBIDDEN, "Credenciales Incorrectas");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

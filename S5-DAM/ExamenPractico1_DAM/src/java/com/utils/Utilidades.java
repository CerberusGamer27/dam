/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.utils;

import java.util.Calendar;
import java.util.Random;

/**
 * Class Name: Utilidades Date: 26 ago. 2022 Version: 1.0 Copyright: Free
 *
 * @author Geovanny Martinez (034519)
 */
public class Utilidades {

    public static String generarCarnet(String apellidos, int anio) {
        Random rand = new Random();
        int upperbound = 9;
        int numeroRandom = rand.nextInt(upperbound);
        String[] primerApellido = apellidos.split(" ");
        String carnet = primerApellido[0] + String.valueOf(anio) + numeroRandom;
        return carnet;
    }

    public static int edad(int anioNacimiento) {
        Calendar cal = Calendar.getInstance();
        int anio = cal.get(Calendar.YEAR) - anioNacimiento;
        return anio;
    }
}

<%-- 
    Document   : ejercicio1
    Created on : 26 ago. 2022, 19:20:30
    Author     : C3rberus
--%>

<%@page import="java.util.Random"%>
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 1</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>
    
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="bootstrap" viewBox="0 0 118 94">
        <title>Bootstrap</title>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z"></path>
    </symbol>
    <symbol id="home" viewBox="0 0 16 16">
        <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
    </symbol>
    <symbol id="speedometer2" viewBox="0 0 16 16">
        <path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4zM3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.389.389 0 0 0-.029-.518z"/>
        <path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A7.988 7.988 0 0 1 0 10zm8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3z"/>
    </symbol>
    <symbol id="table" viewBox="0 0 16 16">
        <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm15 2h-4v3h4V4zm0 4h-4v3h4V8zm0 4h-4v3h3a1 1 0 0 0 1-1v-2zm-5 3v-3H6v3h4zm-5 0v-3H1v2a1 1 0 0 0 1 1h3zm-4-4h4V8H1v3zm0-4h4V4H1v3zm5-3v3h4V4H6zm4 4H6v3h4V8z"/>
    </symbol>
    <symbol id="people-circle" viewBox="0 0 16 16">
        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
    </symbol>
    <symbol id="grid" viewBox="0 0 16 16">
        <path d="M1 2.5A1.5 1.5 0 0 1 2.5 1h3A1.5 1.5 0 0 1 7 2.5v3A1.5 1.5 0 0 1 5.5 7h-3A1.5 1.5 0 0 1 1 5.5v-3zM2.5 2a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 1h3A1.5 1.5 0 0 1 15 2.5v3A1.5 1.5 0 0 1 13.5 7h-3A1.5 1.5 0 0 1 9 5.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zM1 10.5A1.5 1.5 0 0 1 2.5 9h3A1.5 1.5 0 0 1 7 10.5v3A1.5 1.5 0 0 1 5.5 15h-3A1.5 1.5 0 0 1 1 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 9h3a1.5 1.5 0 0 1 1.5 1.5v3a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 9 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3z"/>
    </symbol>
    </svg>

    <body>
        <%!
            public String generarCarnet(String apellidos, int anio){
                Random rand = new Random();
                int upperbound = 9;
                int numeroRandom = rand.nextInt(upperbound);
                String[] primerApellido = apellidos.split(" ");
                String carnet = primerApellido[0]+String.valueOf(anio)+numeroRandom;
                return carnet;
            }
            
            public int edad(int anioNacimiento) {
                Calendar cal = Calendar.getInstance();
                int anio = cal.get(Calendar.YEAR) - anioNacimiento;
                return anio;
            }
        %>
        <div class="container py-4">
            <%
                boolean logeado = false;
                Cookie[] cookies = request.getCookies();

                //Verificar si esta logeado
                if (cookies != null) {
                    for (Cookie c : cookies) {
                        if (c.getName().equals("logeado") && c.getValue().equals("si")) {

                            //Si existe la cookie, el visitante ya esta logeado
                            logeado = true;
                            break;
                        }
                    }
                }
                if (logeado) {
            %>
            <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
                <a href="inicio.jsp" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
                    <span class="fs-4">Examen Practico 1</span>
                </a>

                <ul class="nav nav-pills">
                    <li class="nav-item"><a href="generarWord" class="nav-link active" aria-current="page">Descargar Credenciales</a></li>
                    <li class="nav-item"><a href="index.html" class="nav-link"> <i class="bi bi-box-arrow-left"></i> Salir</a></li>
                </ul>
            </header>
            <a href="inicio.jsp" class="text-center mt-5"><i class="bi bi-chevron-left"></i> Regresar a la lista</a>  
            <br>

            <h3 class="text-sm-center">Datos Personales</h3>
            <form id="formularioTrabajo" action="ejercicio1.jsp" method="post">
                <div class="row g-3">
                    <div class="col-sm-2">
                        <label for="dui" class="form-label">DUI</label>
                        <input type="text" class="form-control" id="dui" name="dui" required title="Ingrese un DUI con guiones"
                               placeholder="01020304-0" pattern="[0-9]{8}-[0-9]{1}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="txtNombre" class="form-label">Nombres</label>
                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Tus Nombres aqui" required>
                        <div class="invalid-feedback">
                            Nombre es requerido.
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="txtApellidos" class="form-label">Apellidos</label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos" placeholder="Tus Nombres aqui" required>
                        <div class="invalid-feedback">
                            Apellido es requerido.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtAnio" class="form-label">Año de Nacimiento</label>
                        <input type="number" class="form-control" id="txtAnio" name="txtAnio" required 
                               title = "Ingrese un año menor al año actual">
                        <div class="invalid-feedback">
                            Año de nacimiento es requerido.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtSueldo" class="form-label">Sueldo $</label>
                        <input type="number" class="form-control" id="txtSueldo" name="txtSueldo" placeholder="$" required 
                               min = "0" title = "Ingrese un sueldo valido">
                        <div class="invalid-feedback">
                            Sueldo es requerido.
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="sexo" class="form-label">Sexo</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="masculino" value="Masculino" required=""
                                   onchange="getValue(this)" >
                            <label class="form-check-label" for="masculino">
                                Masculino
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="femenino" value="Femenino" required=""
                                   onchange="getValue(this)" >
                            <label class="form-check-label" for="femenino">
                                Femenino
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="otro" value="Otro" required=""
                                   onchange="getValue(this)" >
                            <label class="form-check-label" for="otro">
                                Otro
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtOtro" class="form-label">Genero (Otro)</label>
                        <input type="text" class="form-control" id="txtOtro" name="txtOtro" placeholder="Tus Genero aqui" required disabled>
                    </div>
                    
                    <div class="col-sm-3">
                        <label for="cmbCategoria" class="form-label">Categoria</label>
                        <select class="form-select" id="cmbCategoria" name="cmbCategoria" aria-label="" required>
                            <option selected disabled value="">Seleccione un Departamento</option>
                            <option value = "BackEnd" >Desarrollo BackEnd</option>
                            <option value = "FrontEnd" >Desarrollo FrontEnd</option>
                            <option value = "Infraestructura" >Infraestructuras</option>
                            <option value = "Redes" >Redes</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <label for="cmbEspecialidad" class="form-label">Especialidad</label>
                        <select class="form-select" id="cmbEspecialidad" name="cmbEspecialidad" aria-label="" required>
                            <option selected disabled value>Seleccione una Especialidad</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label class="form-label">Intereses</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Programar" name="intereses" id="cProgramar">
                            <label class="form-check-label" for="cProgramar">
                                Programar
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Leer" name="intereses" id="cLeer">
                            <label class="form-check-label" for="cLeer">
                                Leer
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Multimedia" name="intereses" id="cMultimedia">
                            <label class="form-check-label" for="cMultimedia">
                                Multimedia
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Musica" name="intereses" id="cMusica">
                            <label class="form-check-label" for="cMusica">
                                Musica
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Otros" name="intereses" id="cOtro">
                            <label class="form-check-label" for="cOtro">
                                Otros
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="telefono" class="form-label">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" required title="Ingrese un telefono en formato ####-####"
                               placeholder="####-####" pattern="[0-9]{4}-[0-9]{4}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtAfp" class="form-label">AFP</label>
                        <input type="text" class="form-control" id="txtAfp" name="afp" title="Ingrese un numero de AFP"
                               placeholder="###########" pattern="[0-9]{12}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="txtIsss" class="form-label">ISSS</label>
                        <input type="text" class="form-control" id="txtIsss" name="isss" title="Ingrese un numero de ISSS"
                               placeholder="#########" pattern="[0-9]{9}">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label for="direccion" class="form-label">Direccion</label>
                        <textarea class="form-control" id="direccion" name="direccion" rows="2" required=""
                                  placeholder="123 St.John Av"></textarea>
                        <div class="invalid-feedback">
                        </div>
                    </div>


                    <div class="text-center">
                        <button class="w-70 btn btn-outline-primary btn-sm" name="btnEnviar" type="submit">Envíar</button>
                    </div>

                </div>
            </form>
            <%
                String dui = "", nombre = "", apellidos = "", anioNacimiento = "", sueldo = "", genero = "", otro = "", categoria = "", especialidad = "",
                        telefono = "", afp = "", isss = "", direccion = "";
                String[] intereses = {""};
                if (request.getParameter("btnEnviar") != null) {
                    dui = request.getParameter("dui");
                    nombre = request.getParameter("txtNombre");
                    apellidos = request.getParameter("txtApellidos");
                    anioNacimiento = request.getParameter("txtAnio");
                    sueldo = request.getParameter("txtSueldo");
                    genero = request.getParameter("rGenero");
                    otro = request.getParameter("txtOtro");
                    categoria = request.getParameter("cmbCategoria");
                    especialidad = request.getParameter("cmbEspecialidad");
                    intereses = request.getParameterValues("intereses");
                    telefono = request.getParameter("telefono");
                    afp = request.getParameter("afp");
                    isss = request.getParameter("isss");
                    direccion = request.getParameter("direccion");
            %>
            <hr class="mt-5">
            <div class="row mt-5">
                <dl class="row border">
                    <dt class="col-sm-4">
                        DUI:
                    </dt>
                    <dd class="col-sm-8">
                        <%= dui%>
                    </dd>
                    <dt class="col-sm-4">
                        Nombres:
                    </dt>
                    <dd class="col-sm-8">
                        <%= nombre%>
                    </dd>
                    <dt class="col-sm-4">
                        Apellidos:
                    </dt>
                    <dd class="col-sm-8">
                        <%= apellidos%>
                    </dd>
                    <dt class="col-sm-4">
                        Año Nacimiento:
                    </dt>
                    <dd class="col-sm-8">
                        <%= anioNacimiento%>
                    </dd>
                    <dt class="col-sm-4">
                        Edad (Años):
                    </dt>
                    <dd class="col-sm-8">
                        <%= edad((Integer.parseInt(anioNacimiento)))%>
                    </dd>
                    <dt class="col-sm-4">
                        CARNET GENERADO:
                    </dt>
                    <dd class="col-sm-8">
                        <%= generarCarnet(apellidos, Integer.parseInt(anioNacimiento))%>
                    </dd>
                    <dt class="col-sm-4">
                        Sueldo:
                    </dt>
                    <dd class="col-sm-8">
                        $<%= sueldo%>
                    </dd>
                    <dt class="col-sm-4">
                        Genero:
                    </dt>
                    <dd class="col-sm-8">
                        <%= genero%>
                    </dd>
                    <dt class="col-sm-4">
                        Otro Genero (Si Aplica):
                    </dt>
                    <dd class="col-sm-8">
                        <%= otro%>
                    </dd>
                    <dt class="col-sm-4">
                        Categoria:
                    </dt>
                    <dd class="col-sm-8">
                        <%= categoria%>
                    </dd>
                    <dt class="col-sm-4">
                        Especialidad:
                    </dt>
                    <dd class="col-sm-8">
                        <%= especialidad%>
                    </dd>
                    <dt class="col-sm-4">
                        Intereses
                    </dt>
                    <dd class="col-sm-8">
                        <ul class="list-group">
                            <%
                                for (int i = 0; i < intereses.length; i++) {
                                    out.write("<li class='list-group-item'>" + intereses[i] + "</li>");
                                }
                            %>
                        </ul>
                    </dd>
                    <dt class="col-sm-4">
                        Telefono:
                    </dt>
                    <dd class="col-sm-8">
                        <%= telefono%>
                    </dd>
                    <dt class="col-sm-4">
                        AFP:
                    </dt>
                    <dd class="col-sm-8">
                        <%= afp%>
                    </dd>
                    <dt class="col-sm-4">
                        ISSS:
                    </dt>
                    <dd class="col-sm-8">
                        <%= isss%>
                    </dd>
                    <dt class="col-sm-4">
                        Direccion:
                    </dt>
                    <dd class="col-sm-8">
                        <%= direccion%>
                    </dd>
                </dl>
            </div>
            <script>
                    var html_content = '<div class="alert alert-primary" role="alert">' + 'La Edad del usuario es $' + <%= edad((Integer.parseInt(anioNacimiento)))%> +
                            '<br>' + 'El Carnet Generado es : ' + '<%= generarCarnet(apellidos, Integer.parseInt(anioNacimiento))%>' + ' años' + '</div>';
                    Swal.fire({
                        icon: 'success',
                        title: 'Información',
                        html: html_content
                    })
            </script>
            <%
                }
            %>
            <% } else {
                    response.sendError(response.SC_PROXY_AUTHENTICATION_REQUIRED, "Credenciales Incorrectas");
                }
            %>
        </div>
        <script>
            // Selectores
            const cmbCategoria = document.querySelector('#cmbCategoria');
            const cmbEspecialidad = document.querySelector('#cmbEspecialidad');
            const rGenero = document.querySelectorAll('input[name="rGenero"]');
            const txtOtro = document.querySelector('#txtOtro');

            // Cargos
            const cargosBackend = ['PHP', 'JAVA', 'C#'];
            const cargosFrontend = ['VUE', 'JS', 'REACT'];
            const cargosInfraestructura = ['Linux', 'Windows', 'Mac'];
            const cargosRedes = ['Fibra optica', 'Cableado CAT6'];
            
            function getValue(radio){
                console.log(radio.value);
                if(radio.value === "Otro"){
                    //txtOtro.removeAttribute('disabled')
                    txtOtro.disabled = false;
                    txtOtro.value = '';
                } else {
                    txtOtro.disabled = true;
                    txtOtro.value = '';
                }
            }

            //Eventos
            cmbCategoria.addEventListener('change', (e) => {
                console.log(e.target.value);
                while (cmbEspecialidad.options.length > 0) {
                    cmbEspecialidad.remove(0);
                }
                switch (e.target.value) {
                    case "BackEnd":
                        cargosBackend.forEach(cargo => {
                            const opt = document.createElement("option");
                            opt.value = cargo;
                            opt.text = cargo;
                            cmbEspecialidad.add(opt, null);
                        });
                        break;
                    case "FrontEnd":
                        cargosFrontend.forEach(cargo => {
                            const opt = document.createElement("option");
                            opt.value = cargo;
                            opt.text = cargo;
                            cmbEspecialidad.add(opt, null);
                        });
                        break;
                    case "Infraestructura":
                        cargosInfraestructura.forEach(cargo => {
                            const opt = document.createElement("option");
                            opt.value = cargo;
                            opt.text = cargo;
                            cmbEspecialidad.add(opt, null);
                        });
                        break;
                    case "Redes":
                        cargosRedes.forEach(cargo => {
                            const opt = document.createElement("option");
                            opt.value = cargo;
                            opt.text = cargo;
                            cmbEspecialidad.add(opt, null);
                        });
                        break;
                    default:
                        break;
                }

            });
            document.addEventListener('DOMContentLoaded', () => {
                const anioIngreso = document.querySelector("#txtAnio");
                anioIngreso.max = new Date().getFullYear();
                anioIngreso.placeholder = new Date().getFullYear();
            });
        </script>
    </body>
</html>

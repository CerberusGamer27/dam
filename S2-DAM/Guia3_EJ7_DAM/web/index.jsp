<%-- 
    Document   : index
    Created on : 4 ago. 2022, 11:42:13
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 7</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>

    <body>
        <div class="container py-4">
            <header class="pb-3 mb-4 border-bottom">
                <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
                    <span class="fs-1">Ejercicio 7 DAM - 034519</span>
                </a>
            </header>
            <h4>Formulario Completo</h4>
            <br>

            <h3 class="text-sm-center">Datos Personales</h3>
            <form id="formularioTrabajo" action="resultado.jsp" method="post">
                <div class="row g-3">
                    <div class="col-sm-4">
                        <label for="firstName" class="form-label">Nombres</label>
                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Nombres" required>
                        <div class="invalid-feedback">
                            Nombre es requerido.
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="lastName" class="form-label">Apellidos</label>
                        <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Apellidos" required>
                        <div class="invalid-feedback">
                            Nombre es requerido.
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="direccion" class="form-label">Direccion</label>
                        <textarea class="form-control" id="direccion" name="direccion" rows="1"></textarea>
                        <div class="invalid-feedback">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <label for="telefono" class="form-label">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" required
                               pattern="[0-9]*">
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="dui" class="form-label">DUI</label>
                        <input type="text" class="form-control" id="dui" name="dui" required
                               pattern="[0-9]{8}-[0-9]{1}">
                        <div class="invalid-feedback">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <label for="sexo" class="form-label">Sexo</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="masculino" value="Masculino">
                            <label class="form-check-label" for="masculino">
                                Masculino
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="rGenero" id="femenino" value="Femenino">
                            <label class="form-check-label" for="femenino">
                                Femenino
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="form-label">Pasatiempos</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Programar" name="pasatiempos" id="cProgramar">
                            <label class="form-check-label" for="cProgramar">
                                Programar
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Leer" name="pasatiempos" id="cLeer">
                            <label class="form-check-label" for="cLeer">
                                Leer
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Multimedia" name="pasatiempos" id="cMultimedia">
                            <label class="form-check-label" for="cMultimedia">
                                Multimedia
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Musica" name="pasatiempos" id="cMusica">
                            <label class="form-check-label" for="cMusica">
                                Musica
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="Otros" name="pasatiempos" id="cOtro">
                            <label class="form-check-label" for="cOtro">
                                Otros
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="cmbPaises" class="form-label">Paises</label>
                        <select class="form-select" id="cmbPaises" name="cmbPaises" aria-label="">
                            <option selected>Seleccione un pais de la lista</option>
                        </select>
                    </div>

                    <div class="text-center">
                        <button class="w-70 btn btn-outline-primary btn-sm" name="btnEnviar" type="submit">Envíar</button>
                    </div>

                </div>
            </form>
        </div>
        <script>
                    document.addEventListener('DOMContentLoaded', () => {
                        const cmbPaises = document.querySelector("#cmbPaises");
                        const url = 'https://restcountries.com/v2/all';
                        fetch(url).then(res => {
                            return res.json();
                        }).then(data => {
                            let paises = "";
                            data.forEach(country => {
                                paises += '<option value="' + country.translations.es + '">' + country.translations.es + '</option>`';
                            });
                            cmbPaises.innerHTML = paises;
                        }).catch(err => {
                            console.log(err);
                        })
                    });
        </script>
    </body>
</html>

<%-- 
    Document   : resultado
    Created on : 4 ago. 2022, 12:57:32
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 7</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
    </head>

    <body>
        <%
             String dinosaurio = "";
             String nombre = "", apellido = "", direccion = "", telefono = "", dui = "", sexo = "", nacionalidad = "";
             String [] pasatiempos = {""};
            if (request.getParameter("btnEnviar") != null) {
                nombre = request.getParameter("firstName");
                apellido = request.getParameter("lastName");
                direccion = request.getParameter("direccion");
                telefono = request.getParameter("telefono");
                dui = request.getParameter("dui");
                sexo = request.getParameter("rGenero");
                pasatiempos = request.getParameterValues("pasatiempos");
                nacionalidad = request.getParameter("cmbPaises");
            }
        %>
        <div class="container py-4">
            <header class="pb-3 mb-4 border-bottom">
                <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
                    <span class="fs-1">Ejercicio 6 DAM - 034519</span>
                </a>
            </header>
            <div>
                <h4>Resultado</h4>
                <dl class="row">
                    <dt class="col-sm-4">
                        Nombres:
                    </dt>
                    <dd class="col-sm-8">
                        <%= nombre%>
                    </dd>
                    <dt class="col-sm-4">
                        Apellidos
                    </dt>
                    <dd class="col-sm-8">
                        <%= apellido%>
                    </dd>
                    <dt class="col-sm-4">
                        Direccion:
                    </dt>
                    <dd class="col-sm-8">
                        <%= direccion%>
                    </dd>
                    <dt class="col-sm-4">
                        Telefono:
                    </dt>
                    <dd class="col-sm-8">
                        <%= telefono%>
                    </dd>
                    <dt class="col-sm-4">
                        DUI:
                    </dt>
                    <dd class="col-sm-8">
                        <%= dui%>
                    </dd>
                    <dt class="col-sm-4">
                        Sexo:
                    </dt>
                    <dd class="col-sm-8">
                        <%= sexo%>
                    </dd>
                    <dt class="col-sm-4">
                        Pasatiempos:
                    </dt>
                    <dd class="col-sm-8">
                        <ul class="list-group">
                            <%
                                for (int i = 0; i < pasatiempos.length; i++) {
                                    out.write("<li class='list-group-item'>"+pasatiempos[i]+"</li>");
                                }
                            %>
                        </ul>
                    </dd>
                    <dt class="col-sm-4">
                        Nacionalidad:
                    </dt>
                    <dd class="col-sm-8">
                        <%= nacionalidad%>
                    </dd>
                </dl>
            </div>
            <br>
        </div>
    </body>
</html>

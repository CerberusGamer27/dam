<%-- 
    Document   : index
    Created on : 2 ago. 2022, 22:01:17
    Author     : C3rberus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 5</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>

    <body>
        <div class="container py-4">
            <header class="pb-3 mb-4 border-bottom">
                <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
                    <span class="fs-1">Ejercicio 5 DAM - 034519</span>
                </a>
            </header>
            <h4>Asignacion de Presupuesto</h4>
            <br>
            <form class="row g-3 validar" id="form1" action="resultado.jsp" method="post">
                <div class="col-md-12"> 
                    <div class="row mb-3">
                        <label for="txtPresupuesto" class="col-sm-2 col-form-label">Presupuesto:</label>
                        <div class="col-sm-5">
                            <input type="number" step="0.01" class="form-control" id="txtPresupuesto" name="txtPresupuesto" required>
                        </div>
                    </div>

                </div>
                <div class="col-md-2">
                    <label for="" class="form-label">Ginecobstetricia</label>
                </div>
                <div class="col-md-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="presupuestoGine" id="gine1" value="0.40" required>
                        <label class="form-check-label" for="gine1">40%</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="presupuestoGine" id="gine2" value="0.30" required>
                        <label class="form-check-label" for="gine2">30%</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="" class="form-label">Traumatologia</label>
                </div>
                <div class="col-md-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="presupuestoTraumo" id="traumo1" value="0.40" required>
                        <label class="form-check-label" for="traumo1">40%</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="presupuestoTraumo" id="traumo2" value="0.30" required>
                        <label class="form-check-label" for="traumo2">30%</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="" class="form-label">Pediatria</label>
                </div>
                <div class="col-md-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="presupuestoPedia" id="pedia1" value="0.40" required>
                        <label class="form-check-label" for="pedia1">40%</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="presupuestoPedia" id="pedia2" value="0.30" required>
                        <label class="form-check-label" for="pedia2">30%</label>
                    </div>
                </div>
                <div class="col-12">
                    <button class="btn btn-primary" id="btnEnviar" name="btnEnviar" type="submit">Enviar</button>
                </div>
            </form>
        </div>
        <script>
                    const formulario = document.getElementById("form1");
                    formulario.addEventListener("submit", (e) => {
                        e.preventDefault();
                        let gineco = document.querySelector('input[name="presupuestoGine"]:checked').value;
                        let trauma = document.querySelector('input[name="presupuestoTraumo"]:checked').value;
                        let pedia = document.querySelector('input[name="presupuestoPedia"]:checked').value;
                        console.info(gineco);
                        console.info(trauma);
                        console.info(pedia);
                        console.info((parseFloat(gineco) + parseFloat(trauma) + parseFloat(pedia))*100)
                        if ((parseFloat(gineco) + parseFloat(trauma) + parseFloat(pedia))*100 > 100 || (parseFloat(gineco) + parseFloat(trauma) + parseFloat(pedia))*100 < 100) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error en la Asignacion',
                                text: 'Necesita modificar los presupuestos para que la suma sea 100%'
                            })
                        } else {
                            formulario.submit();
                        }
                    });
        </script>
    </body>
</html>
